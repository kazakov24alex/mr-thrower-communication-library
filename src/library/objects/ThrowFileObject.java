package library.objects;

import library.codes.PacketCodes;
import library.wrappers.MD5;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * POJO-class for storing file to throwing.
 * @author Alex Kazakov
 * @version TESTED (coverage 100%)
 */
public class ThrowFileObject extends BaseProtocolObject {

    private Date mTimeID;
    private String mFilename;
    private long mFileSize;
    private MD5 mMD5;

    private String mPath;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public ThrowFileObject(Date timeID, String filename, long filesize, MD5 md5) {
        code = PacketCodes.THROW_FILE;

        mTimeID = timeID;
        mFilename = filename;
        mFileSize = filesize;
        mMD5 = md5;
    }


//======================================================================================================================
// SETTERS
//======================================================================================================================

    public void setPath(String path) {
        mPath = path;
    }


//======================================================================================================================
// GETTERS
//======================================================================================================================

    public Date getTimeID() {
        return mTimeID;
    }

    public long getFileSize() {
        return mFileSize;
    }

    public String getFilename() {
        return mFilename;
    }

    public MD5 getMD5() {
        return mMD5;
    }

    public String getPath() {
        return mPath;
    }

    @Override
    public String toString() {
        String str =  "[THROW FILE] ";
        if(address != null)
            str += "from " + address.toString();

        str += "\n"
                + "\tTimeID: " + mTimeID.toString() + "\n"
                + "\tName:   " + mFilename + "\n"
                + "\tSize:   " + mFileSize + "\n"
                + "\tMD5:    " + mMD5.toString() + "\n";

        return str;
    }

    public String getUUIDstart() {
        Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
        calendar.setTime(mTimeID);   // assigns calendar to given date

        String uuidStart = "";
        int hours = calendar.get(Calendar.HOUR); // gets hour in 24h format
        int minutes = calendar.get(Calendar.MINUTE);        // gets hour in 12h format
        int seconds = calendar.get(Calendar.SECOND);
        int millis = calendar.get(Calendar.MILLISECOND);

        if(hours < 10)
            uuidStart += hours;
        else
            uuidStart += hours % 2;

        if(minutes < 10)
            uuidStart += "0" + minutes;
        else
            uuidStart += minutes;

        if(seconds < 10)
            uuidStart += "0" + seconds;
        else
            uuidStart += seconds;

        if(millis < 10)
            uuidStart += "00" + millis;
        else if(millis < 100)
            uuidStart += "0" + millis;
        else
            uuidStart += millis;

        return uuidStart;
    }

}
