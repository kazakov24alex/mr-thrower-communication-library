package library.objects;

import library.BytePacket;
import library.ByteUtils;
import library.codes.PacketCodes;
import library.exceptions.BuildPacketException;
import library.exceptions.EncodingException;
import library.exceptions.ParsePacketException;
import library.wrappers.MD5;
import library.wrappers.MacAddress;

import javax.crypto.Mac;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * POJO-class for storing file throw request.
 * @author Alex Kazakov
 * @version TESTED (coverage 100%)
 */
public class ThrowRequestObject extends BaseProtocolObject {

    private List<ThrowFileObject> mThrowFilesList;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public ThrowRequestObject() {
        code = PacketCodes.THROW_REQUEST;

        mThrowFilesList = new ArrayList<>();
    }

    public ThrowRequestObject(BytePacket packet, MacAddress macAddress) throws ParsePacketException {
        code = PacketCodes.THROW_REQUEST;

        ThrowRequestObject request = parseBytePacket(packet);
        mThrowFilesList = new ArrayList<>(request.getThrowFiles());

        setAddress(macAddress);
    }

    public BytePacket getBytePacket() throws BuildPacketException {
        return buildBytePacket();
    }


//======================================================================================================================
// SETTERS
//======================================================================================================================

    public void setAddress(MacAddress address) {
        this.address = address;

        for (ThrowFileObject throwFile: mThrowFilesList) {
            throwFile.setAddress(address);
        }
    }

    public void addThrowFile(ThrowFileObject throwFile) {
        mThrowFilesList.add(throwFile);
    }

//======================================================================================================================
// GETTERS
//======================================================================================================================

    public List<ThrowFileObject> getThrowFiles() {
        return mThrowFilesList;
    }

    @Override
    public String toString() {
        String str =  "[THROW REQUEST] ";
        if(address != null)
            str += "from " + address.toString();

        for(ThrowFileObject tFile : mThrowFilesList) {
            str += "\n"
                    + "\tTimeID: " + tFile.getTimeID().toString() + " \tName: " + tFile.getFilename();
        }

        return str;
    }


//======================================================================================================================
// PROTOCOL
// code (1b) + LENGTH (2b) + FILE_NUM (2b)
//                            + TIME_ID (8b) + FILE_SIZE (8b) + MD5 (16b) + FILENAME_LEN (2b) + FILENAME) // * FILE_NUM
//======================================================================================================================

    private BytePacket buildBytePacket() throws BuildPacketException {
        if(mThrowFilesList.size() < 1) {
            throw new BuildPacketException("There are no files to transfer");
        }

        // code (1b)
        BytePacket packet = new BytePacket(PacketCodes.THROW_REQUEST);

        // LENGTH (2b)
        int length = 1 + 2 + 2;

        for(ThrowFileObject tFile : mThrowFilesList) {
            length += 8 + 8 + 16 + 2 + ByteUtils.getBytesNum(tFile.getFilename());
        }
        packet.appendShort((short) length);

        // FILE_NUM (2b)
        packet.appendShort((short) mThrowFilesList.size());

        for(ThrowFileObject tFile : mThrowFilesList) {
            // TIME_ID (8b)
            packet.appendLong(tFile.getTimeID().getTime());
            // FILE_SIZE (8b)
            packet.appendLong(tFile.getFileSize());
            // MD5 (16b)
            packet.append(tFile.getMD5().getBytes());
            // FILENAME_LEN (2b)
            packet.appendShort((short) ByteUtils.getBytesNum(tFile.getFilename()));
            // FILENAME
            packet.appendString(tFile.getFilename());
        }

        return packet;
    }

    private ThrowRequestObject parseBytePacket(BytePacket packet) throws ParsePacketException {
        try {
            packet.storeData();
            ThrowRequestObject request = new ThrowRequestObject();

            Byte code = packet.takeByte();
            int length = packet.takeShort();

            int fileNum = packet.takeShort();

            for(int i = 0; i < fileNum; i++) {
                long timeID = packet.takeLong();
                long filesize = packet.takeLong();
                Byte[] md5bytes = packet.takeBytes(16);
                short filenameLen = packet.takeShort();
                String filename = packet.takeString(filenameLen);

                request.addThrowFile(new ThrowFileObject(new Date(timeID), filename, filesize, new MD5(ByteUtils.toPrimitives(md5bytes))));
            }

            packet.restoreData();

            return request;

        } catch(EncodingException e) {
            throw new ParsePacketException(e.getMessage());
        }
    }

}
