package library.objects;

import library.BytePacket;
import library.codes.PacketCodes;
import library.exceptions.EncodingException;
import library.exceptions.ParsePacketException;
import library.wrappers.MacAddress;

import java.util.Date;


/**
 * POJO-class for storing file throw end acknowledgement.
 * @author Alex Kazakov
 * @version TESTED (coverage 100%)
 */
public class ThrowEndAckObject extends BaseProtocolObject {

    private Date mTimeID;
    private boolean mSuccess;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public ThrowEndAckObject(Date timeID, boolean success) {
        code = PacketCodes.THROW_END_ACK;

        mTimeID = timeID;
        mSuccess = success;
    }

    public ThrowEndAckObject(BytePacket packet, MacAddress sender) throws ParsePacketException {
        code = PacketCodes.THROW_END_ACK;
        address = sender;

        ThrowEndAckObject throwEndAck = parseBytePacket(packet);
        mTimeID = throwEndAck.getTimeID();
        mSuccess = throwEndAck.isSuccess();
    }

    public BytePacket getBytePacket() {
        return buildBytePacket();
    }


//======================================================================================================================
// GETTERS
//======================================================================================================================

    public Date getTimeID() {
        return mTimeID;
    }

    public boolean isSuccess() {
        return mSuccess;
    }

    @Override
    public String toString() {
        String str =  "[THROW END ACK] ";
        if(address != null)
            str += "from " + address.toString();

        str += "\n"
                + "\tTimeID:  " + mTimeID.toString() + "\n"
                + "\tSuccess: " + mSuccess;

        return str;
    }


//======================================================================================================================
// PROTOCOL
// CODE (1b) + LENGTH (2b) + TIME_ID (8b) + SUCCESS (1b)
//======================================================================================================================

    private BytePacket buildBytePacket() {
        // CODE (1b)
        BytePacket packet = new BytePacket(PacketCodes.THROW_END_ACK);

        // LENGTH (2b)
        int length = 1 + 2 + 8 + 1;
        packet.appendShort((short) length);
        // TIME_ID (8b)
        packet.appendLong(mTimeID.getTime());
        // SUCCESS (1b)
        if(isSuccess())
            packet.appendByte((byte) 0);
        else
            packet.appendByte((byte) 1);

        return packet;
    }

    private ThrowEndAckObject parseBytePacket(BytePacket packet) throws ParsePacketException {
        try {
            packet.storeData();

            Byte code = packet.takeByte();
            int length = packet.takeShort();
            long timeID = packet.takeLong();
            byte status = packet.takeByte();
            boolean success = false;
            if(status == 0)
                success = true;

            packet.restoreData();

            return new ThrowEndAckObject(new Date(timeID), success);

        } catch(EncodingException e) {
            throw new ParsePacketException(e.getMessage());
        }
    }

}
