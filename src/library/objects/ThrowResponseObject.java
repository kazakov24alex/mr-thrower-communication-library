package library.objects;

import library.BytePacket;
import library.codes.PacketCodes;
import library.exceptions.EncodingException;
import library.exceptions.ParsePacketException;
import library.wrappers.MacAddress;

import java.util.Date;

/**
 * POJO-class for storing file throw response.
 * @author Alex Kazakov
 * @version TESTED (coverage 100%)
 */
public class ThrowResponseObject extends BaseProtocolObject {

    public static final int NEGATIVE_RESPONSE = -1;

    private Date mTimeID;
    private int mPort;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public ThrowResponseObject(Date timeID, int port) {
        code = PacketCodes.THROW_RESPONSE;

        mTimeID = timeID;
        mPort = port;
    }

    public ThrowResponseObject(BytePacket packet, MacAddress sender) throws ParsePacketException {
        code = PacketCodes.THROW_RESPONSE;
        address = sender;

        ThrowResponseObject response = parseBytePacket(packet);
        mTimeID = response.getTimeID();
        mPort = response.getPort();
    }

    public BytePacket getBytePacket() {
        return buildBytePacket();
    }


//======================================================================================================================
// GETTERS
//======================================================================================================================

    public boolean isNegative() {
        return mPort == NEGATIVE_RESPONSE;
    }

    public Date getTimeID() {
        return mTimeID;
    }

    public int getPort() {
        return mPort;
    }

    @Override
    public String toString() {
        String str =  "[THROW RESPONSE] ";
        if(address != null)
            str += "from " + address.toString();

        str += "\n"
                + "\tTimeID: " + mTimeID.toString() + "\n"
                + "\tPort:   " + mPort;

        return str;
    }


//======================================================================================================================
// PROTOCOL
// CODE (1b) + LENGTH (2b) + TIME_ID (8b) + PORT (4b)
//======================================================================================================================

    private BytePacket buildBytePacket() {
        // CODE (1b)
        BytePacket packet = new BytePacket(PacketCodes.THROW_RESPONSE);

        // LENGTH (2b)
        int length = 1 + 2 + 8 + 4;
        packet.appendShort((short) length);
        // TIME_ID (8b)
        packet.appendLong(mTimeID.getTime());
        // PORT (4b)
        packet.appendInt(mPort);

        return packet;
    }

    private ThrowResponseObject parseBytePacket(BytePacket packet) throws ParsePacketException {
        try {
            packet.storeData();

            Byte code = packet.takeByte();
            int length = packet.takeShort();
            long timeID = packet.takeLong();
            int port = packet.takeInt();

            packet.restoreData();

            return new ThrowResponseObject(new Date(timeID), port);

        } catch(EncodingException e) {
            throw new ParsePacketException(e.getMessage());
        }
    }

}
