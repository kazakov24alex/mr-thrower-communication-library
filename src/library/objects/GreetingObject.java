package library.objects;

import library.BytePacket;
import library.ByteUtils;
import library.codes.PacketCodes;
import library.exceptions.BuildPacketException;
import library.exceptions.EncodingException;
import library.exceptions.ParsePacketException;
import library.wrappers.IpAddress;
import library.wrappers.MacAddress;


/**
 * POJO-class for storing information on a specific greeting.
 * @author Alex Kazakov
 * @version TESTED (coverage 100%)
 */
public class GreetingObject extends BaseProtocolObject {

    public final static byte OS_CODE_WINDOWS = 1;
    public final static byte OS_CODE_ANDROID = 2;

    public final static String OS_NAME_WINDOWS = "Windows";
    public final static String OS_NAME_ANDROID = "Android";

    private IpAddress mIpAddress;
    private MacAddress mMacAddress;
    private String mName;
    private String mDevice;
    private Byte mOs;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public GreetingObject(MacAddress macAddress, IpAddress ipAddress, String name, String device, Byte os) {
        code = PacketCodes.GREETING;
        mMacAddress = macAddress;
        mIpAddress = ipAddress;
        mName = name;
        mDevice = device;
        mOs = os;
    }

    public GreetingObject(BytePacket packet) throws ParsePacketException{
        code = PacketCodes.GREETING;

        GreetingObject greeting = parseBytePacket(packet);
        mMacAddress = greeting.getMacAddress();
        mIpAddress = greeting.getIpAddress();
        mName = greeting.getName();
        mDevice = greeting.getDevice();
        mOs = greeting.getOs();
    }

    public BytePacket getBytePacket() throws BuildPacketException {
        return buildBytePacket(PacketCodes.GREETING);
    }

    public BytePacket getAckBytePacket() throws BuildPacketException {
        return buildBytePacket(PacketCodes.GREETING_ACK);
    }

//======================================================================================================================
// GETTERS
//======================================================================================================================


    public MacAddress getMacAddress() {
        return mMacAddress;
    }

    public IpAddress getIpAddress() { return mIpAddress; }

    public String getName() {
        return mName;
    }

    public String getDevice() {
        return mDevice;
    }

    public Byte getOs() {
        return mOs;
    }

    @Override
    public String toString() {
        String str = "[GREETING] from " + mMacAddress.toString() + "\n"
                + "\tName:   " + mName + "\n"
                + "\tIP:     " + mIpAddress.toString() + "\n"
                + "\tDevice: " + mDevice + "\n"
                + "\tOS:     ";

        switch (mOs) {
            case OS_CODE_WINDOWS:
                str += OS_NAME_WINDOWS;
                break;
            case OS_CODE_ANDROID:
                str += OS_NAME_ANDROID;
                break;
        }

        return str;
    }


//======================================================================================================================
// PROTOCOL
// code (1b) + LENGTH (2b) + MAC_ADDRESS (6b) + IP_ADDRESS (4b) + NAME_LEN (2b) + NAME + DEVICE_LEN (2b) + DEVICE + OS (1b)
//======================================================================================================================

    private BytePacket buildBytePacket(byte code) throws BuildPacketException {
        // code (1b)
        BytePacket packet = new BytePacket(code);

        // LENGTH (2b)
        int length = 1 + 2 + 6 + 4 + 2 + ByteUtils.getBytesNum(mName) + 2 + ByteUtils.getBytesNum(mDevice) + 1;
        packet.appendShort((short) length);
        // MAC_ADDRESS (6b)
        packet.append(mMacAddress.getBytes());
        // IP_ADDRESS (4b)
        packet.append(mIpAddress.getBytes());
        // NAME_LEN (2b)
        packet.appendShort((short) ByteUtils.getBytesNum(mName));
        // NAME
        packet.appendString(mName);
        // DEVICE_LEN (2b)
        packet.appendShort((short) ByteUtils.getBytesNum(mDevice));
        // DEVICE
        packet.appendString(mDevice);
        // OS (1b)
        packet.appendByte(mOs);

        return packet;
    }

    private GreetingObject parseBytePacket(BytePacket packet) throws ParsePacketException {
        try {
            packet.storeData();

            Byte code = packet.takeByte();
            int length = packet.takeShort();
            MacAddress macAddress = new MacAddress(packet.takeBytes(6));
            IpAddress ipAddress = new IpAddress(packet.takeBytes(4));
            int nameLen = packet.takeShort();
            String name = packet.takeString(nameLen);
            int deviceLen = packet.takeShort();
            String device = packet.takeString(deviceLen);
            Byte os = packet.takeByte();

            packet.restoreData();

            return new GreetingObject(macAddress, ipAddress, name, device, os);

        } catch(EncodingException e) {
            throw new ParsePacketException(e.getMessage());
        }
    }

}
