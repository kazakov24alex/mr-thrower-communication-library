package library.objects;

import library.BytePacket;
import library.codes.PacketCodes;
import library.exceptions.EncodingException;
import library.exceptions.ParsePacketException;
import library.wrappers.MacAddress;


/**
 * POJO-class for storing information on a specific goodbye.
 * @author Alex Kazakov
 * @version TESTED (coverage 100%)
 */
public class GoodbyeObject extends BaseProtocolObject {


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public GoodbyeObject(MacAddress macAddress) {
        code = PacketCodes.GOODBYE;
        address = macAddress;
    }

    public GoodbyeObject(BytePacket packet) throws ParsePacketException {
        code = PacketCodes.GOODBYE;
        GoodbyeObject goodbye = parseBytePacket(packet);

        address = goodbye.getMac();
    }

    public BytePacket getBytePacket() {
        return buildBytePacket();
    }

//======================================================================================================================
// GETTERS
//======================================================================================================================

    public MacAddress getMac() {
        return address;
    }

    @Override
    public String toString() {
        return "[GOODBYE] from " + address.toString();
    }


//======================================================================================================================
// PROTOCOL
// code (1b) + LENGTH (2b) + MAC_ADDRESS (6b)
//======================================================================================================================

    private BytePacket buildBytePacket() {
        // code (1b)
        BytePacket packet = new BytePacket(PacketCodes.GOODBYE);

        // LENGTH (2b)
        int length = 1 + 2 + 6;
        packet.appendShort((short) length);
        // MAC_ADDRESS (6b)
        packet.append(address.getBytes());

        return packet;
    }

    private GoodbyeObject parseBytePacket(BytePacket packet) throws ParsePacketException {
        try {
            packet.storeData();

            Byte code = packet.takeByte();
            int length = packet.takeShort();
            MacAddress macAddress = new MacAddress(packet.takeBytes(6));

            packet.restoreData();

            return new GoodbyeObject(macAddress);

        } catch(EncodingException e) {
            throw new ParsePacketException(e.getMessage());
        }
    }

}
