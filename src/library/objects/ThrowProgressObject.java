package library.objects;

import library.BytePacket;
import library.codes.PacketCodes;
import library.exceptions.EncodingException;
import library.exceptions.ParsePacketException;
import library.wrappers.MacAddress;

import java.util.Date;

/**
 * POJO-class for storing file throw progress.
 * @author Alex Kazakov
 * @version TESTED (coverage 100%)
 */
public class ThrowProgressObject extends BaseProtocolObject {

    private Date mTimeID;
    private long mValue;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public ThrowProgressObject(Date timeID, long value) {
        code = PacketCodes.THROW_PROGRESS;

        mTimeID = timeID;
        mValue = value;
    }

    public ThrowProgressObject(BytePacket packet, MacAddress sender) throws ParsePacketException {
        code = PacketCodes.THROW_PROGRESS;
        address = sender;

        ThrowProgressObject progress = parseBytePacket(packet);
        mTimeID = progress.getTimeID();
        mValue = progress.getValue();
    }

    public BytePacket getBytePacket() {
        return buildBytePacket();
    }


//======================================================================================================================
// GETTERS
//======================================================================================================================

    public Date getTimeID() {
        return mTimeID;
    }

    public long getValue() {
        return mValue;
    }

    @Override
    public String toString() {
        String str = "[THROW PROGRESS] bytes: " + mValue;

        if(address != null)
            str += "\tfrom " + address.toString();

        str += "\tTimeID:   " + mTimeID.toString();

        return str;
    }


//======================================================================================================================
// PROTOCOL
// CODE (1b) + LENGTH (2b) + TIME_ID (8b) + PROGRESS (8b)
//======================================================================================================================

    private BytePacket buildBytePacket() {
        // CODE (1b)
        BytePacket packet = new BytePacket(PacketCodes.THROW_PROGRESS);

        // LENGTH (2b)
        int length = 1 + 2 + 8 + 8;
        packet.appendShort((short) length);
        // TIME_ID (8b)
        packet.appendLong(mTimeID.getTime());
        // PROGRESS (8b)
        packet.appendLong(mValue);

        return packet;
    }

    private ThrowProgressObject parseBytePacket(BytePacket packet) throws ParsePacketException {
        try {
            packet.storeData();

            Byte code = packet.takeByte();
            int length = packet.takeShort();
            long timeID = packet.takeLong();
            long progress = packet.takeLong();

            packet.restoreData();

            return new ThrowProgressObject(new Date(timeID), progress);

        } catch(EncodingException e) {
            throw new ParsePacketException(e.getMessage());
        }
    }

}
