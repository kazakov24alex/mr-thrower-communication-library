package library.objects;

import library.BytePacket;
import library.ByteUtils;
import library.codes.PacketCodes;
import library.exceptions.EncodingException;
import library.exceptions.ParsePacketException;
import library.wrappers.MacAddress;

import java.util.Date;


/**
 * POJO-class for storing information on a confirmation of a text message.
 * @author Alex Kazakov
 * @version TESTED (coverage 100%)
 */
public class MessageAckObject extends BaseProtocolObject {

    public static final byte CODE_SUCCESS = 0;
    public static final byte CODE_FAILURE = 1;

    private Date mTimeID;
    private byte mErrorCode;
    private String mErrorMessage;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public MessageAckObject(Date timeID, byte errorCode, String errorMessage) {
        code = PacketCodes.MESSAGE_ACK;

        mTimeID = timeID;
        mErrorCode = errorCode;
        mErrorMessage = errorMessage;
    }

    public MessageAckObject(BytePacket packet, MacAddress sender) throws ParsePacketException {
        code = PacketCodes.MESSAGE_ACK;
        address = sender;

        MessageAckObject messageAck = parseBytePacket(packet);
        mTimeID = messageAck.getTimeID();
        mErrorCode = messageAck.getErrorCode();
        mErrorMessage = messageAck.getErrorMessage();
    }

    public BytePacket getBytePacket() {
        return buildBytePacket();
    }


//======================================================================================================================
// GETTERS
//======================================================================================================================

    public boolean isNoErrors() {
        return mErrorCode == CODE_SUCCESS;
    }

    public Date getTimeID() {
        return mTimeID;
    }

    public byte getErrorCode() {
        return mErrorCode;
    }

    public String getErrorMessage() {
        return mErrorMessage;
    }

    @Override
    public String toString() {
        String str =  "[MESSAGE ACK] ";
        if(address != null)
            str += "from " + address.toString();

        str += "\n"
                + "\tTimeID:     " + mTimeID.toString() + "\n"
                + "\tErrCode:    " + mErrorCode + "\n"
                + "\tErrMessage: " + mErrorMessage;

        return str;
    }


//======================================================================================================================
// PROTOCOL
// CODE (1b) + LENGTH (2b) + TIME_ID (8b) + ERROR_CODE (1b) + ERROR_MESSAGE_LEN (2b) + ERROR_MESSAGE
//======================================================================================================================

    private BytePacket buildBytePacket() {
        // CODE (1b)
        BytePacket packet = new BytePacket(PacketCodes.MESSAGE_ACK);

        // LENGTH (2b)
        int length = 1 + 2 + 8 + 1 + 2 + ByteUtils.getBytesNum(mErrorMessage);
        packet.appendShort((short) length);
        // TIME_ID (8b)
        packet.appendLong(mTimeID.getTime());
        // ERROR_CODE (1b)
        packet.appendByte(mErrorCode);
        // ERROR_MESSAGE_LEN (2b)
        packet.appendShort((short) ByteUtils.getBytesNum(mErrorMessage));
        // ERROR_MESSAGE
        packet.appendString(mErrorMessage);

        return packet;
    }

    private MessageAckObject parseBytePacket(BytePacket packet) throws ParsePacketException {
        try {
            packet.storeData();

            Byte code = packet.takeByte();
            int length = packet.takeShort();
            long timeID = packet.takeLong();
            Byte errorCode = packet.takeByte();
            int errorMesLen = packet.takeShort();
            String errorMes = packet.takeString(errorMesLen);

            packet.restoreData();

            return new MessageAckObject(new Date(timeID), errorCode, errorMes);

        } catch(EncodingException e) {
            throw new ParsePacketException(e.getMessage());
        }
    }

}
