package library.objects;

import library.BytePacket;
import library.ByteUtils;
import library.codes.PacketCodes;
import library.exceptions.EncodingException;
import library.exceptions.ParsePacketException;
import library.wrappers.MacAddress;

import javax.crypto.Mac;
import java.util.Date;


/**
 * POJO-class for storing information on a mText mText.
 * @author Alex Kazakov
 * @version TESTED (coverage 100%)
 */
public class MessageObject extends BaseProtocolObject {

    private Date mTimeID;
    private String mText;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public MessageObject(Date timeID, String text) {
        code = PacketCodes.MESSAGE;
        mTimeID = timeID;
        mText = text;
    }

    public MessageObject(BytePacket packet, MacAddress sender) throws ParsePacketException {
        code = PacketCodes.MESSAGE;
        address = sender;

        MessageObject message = parseBytePacket(packet);
        this.mTimeID = message.getTimeID();
        this.mText = message.getText();
    }

    public BytePacket getBytePacket() {
        return buildBytePacket();
    }


//======================================================================================================================
// GETTERS
//======================================================================================================================

    public String getText() {
        return mText;
    }

    public Date getTimeID() {
        return mTimeID;
    }

    @Override
    public String toString() {
        String str = "[MESSAGE] ";
        if(address != null)
            str += "from " + address.toString();

        str += "\n"
                + "\tTimeID: " + mTimeID.toString() + "\n"
                + "\tText:   " + mText;

        return str;
    }


//======================================================================================================================
// PROTOCOL
// CODE (1b) + LENGTH (2b) + TIME_ID (8b) +  TEXT_LEN (2b) + TEXT
//======================================================================================================================

    private BytePacket buildBytePacket() {
        // CODE (1b)
        BytePacket packet = new BytePacket(PacketCodes.MESSAGE);

        // LENGTH (2b)
        int length = 1 + 2 + 8 + 2 + ByteUtils.getBytesNum(mText);
        packet.appendShort((short) length);
        // TIME_ID (8b)
        packet.appendLong(mTimeID.getTime());
        // TEXT_LEN (2b)
        packet.appendShort((short) ByteUtils.getBytesNum(mText));
        // TEXT
        packet.appendString(mText);

        return packet;
    }

    private MessageObject parseBytePacket(BytePacket packet) throws ParsePacketException {
        try {
            packet.storeData();

            Byte code = packet.takeByte();
            int length = packet.takeShort();
            long timeID = packet.takeLong();
            int messageLen = packet.takeShort();
            String message = packet.takeString(messageLen);

            packet.restoreData();

            return new MessageObject(new Date(timeID), message);

        } catch(EncodingException e) {
            throw new ParsePacketException(e.getMessage());
        }
    }

}
