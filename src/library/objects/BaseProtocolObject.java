package library.objects;

import library.wrappers.MacAddress;

public class BaseProtocolObject {

    protected byte code;

    protected MacAddress address;


//======================================================================================================================
// SETTERS
//======================================================================================================================

    public void setAddress(MacAddress address) {
        this.address = address;
    }


//======================================================================================================================
// GETTERS
//======================================================================================================================

    public byte getCode() {
        return code;
    }

    public MacAddress getAddress() {
        return address;
    }

}
