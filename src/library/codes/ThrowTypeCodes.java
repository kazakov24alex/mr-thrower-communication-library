package library.codes;

public class ThrowTypeCodes {

    public static final byte FILE   = 1;
    public static final byte FOLDER = 2;

    public static final String FILE_TITLE   = "FILE";
    public static final String FOLDER_TITLE = "FOLDER";

}
