package library.codes;


/**
 * Static class for storing codes of communication packages of information.
 * @author Alex Kazakov
 */
public class PacketCodes {

    public static final byte GREETING = 1;
    public static final byte GREETING_ACK = 2;

    public static final byte GOODBYE = 3;

    public static final byte MESSAGE = 4;
    public static final byte MESSAGE_ACK = 5;

    public static final byte THROW_FILE = 10;

    public static final byte THROW_REQUEST = 6;
    public static final byte THROW_RESPONSE = 7;
    public static final byte THROW_PROGRESS = 8;
    public static final byte THROW_END_ACK = 9;


    // REQUEST CODES
    public static final byte REQUEST_CONTACTS = 10;

}
