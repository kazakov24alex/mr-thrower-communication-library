package library.codes;

public class ThrowStateCodes {

    public static final byte STATE_REQUEST      = 1;
    public static final byte STATE_REJECTED     = 2;
    public static final byte STATE_PROCESS      = 3;
    public static final byte STATE_INTERRUPTED  = 4;
    public static final byte STATE_FINISHED     = 5;
    public static final byte STATE_DELETED      = 6;

    public static final String STATE_REQUEST_TITLE      = "REQUEST";
    public static final String STATE_REJECTED_TITLE     = "REJECTED";
    public static final String STATE_PROCESS_TITLE      = "PROCESS";
    public static final String STATE_INTERRUPTED_TITLE  = "INTERRUPTED";
    public static final String STATE_FINISHED_TITLE     = "FINISHED";
    public static final String STATE_DELETED_TITLE      = "DELETED";

}
