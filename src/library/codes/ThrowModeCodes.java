package library.codes;

public class ThrowModeCodes {

    public static final byte WIFI_NETWORK   = 1;
    public static final byte WIFI_DIRECT    = 2;
    public static final byte BLUETOOTH      = 3;

    public static final String WIFI_NETWORK_TITLE   = "WIFI NETWORK";
    public static final String WIFI_DIRECT_TITLE    = "WIFI DIRECT";
    public static final String BLUETOOTH_TITLE      = "BLUETOOTH";

}
