package library.codes;

public class OsCodes {

    public static final byte WINDOWS = 1;
    public static final byte ANDROID = 2;

    public static final String WINDOWS_TITLE = "WINDOWS";
    public static final String ANDROID_TITLE = "ANDROID";

}
