package library.codes;

public class DirectionCodes {

    public static final byte INCOMING   = 1;
    public static final byte OUTCOMING  = 2;

    public static final String INCOMING_TITLE   = "IN";
    public static final String OUTCOMING_TITLE  = "OUT";

}
