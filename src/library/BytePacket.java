package library;

import library.exceptions.EncodingException;

import java.util.Vector;


/**
 * Class-wrapper for storage byte packet.
 * @author Alex Kazakov
 * @version TESTED (coverage 100%)
 */
public class BytePacket {

    /** vector for storage byte-message of packet */
    private Vector<Byte> mByteVector;
    /** vector for reserve copy byte-message of packet */
    private Vector<Byte> mReserveByteVector;


//======================================================================================================================
// CONSTRUCTORS
//======================================================================================================================

    /**
     * The constructor of byte packet initializing by code of message.
     * @param code - protocol-code of message
     */
    public BytePacket(Byte code) {
        mByteVector = new Vector<Byte>();
        appendByte(code);
    }

    /**
     * The constructor of byte packet initializing by code of message.
     * @param bytes - byte-message
     */
    public BytePacket(byte[] bytes) {
        mByteVector = new Vector<Byte>();
        append(bytes);
    }

    /**
     * The constructor of byte packet initializing by code, length and message.
     * @param code - protocol-code of message
     * @param length - number of bytes in the message
     * @param message - byte-content of message
     */
    public BytePacket(byte code, byte[] length, byte[] message) {
        mByteVector = new Vector<Byte>();
        appendByte(code);
        append(length);
        append(message);
    }


//======================================================================================================================
// GETTERS
//======================================================================================================================

    /**
     * Return byte-content of packet in byte array form.
     * @return byte-content of packet
     */
    public byte[] getData() {
        Byte[] bytes = mByteVector.toArray(new Byte[mByteVector.size()]);
        return ByteUtils.toPrimitives(bytes);
    }

    /**
     * Return the number of bytes of message.
     * @return the number of bytes of message
     */
    public int size() {
        return mByteVector.size();
    }

    /**
     * Return byte from a spec3ific position in the message.
     * @param at position of byte
     * @exception EncodingException incorrect position
     * @return the number of bytes of message
     */
    public Byte getByte(int at) throws EncodingException {
        if(at >= size() || at < 0)
            throw new EncodingException("Getting byte from position = " + at + " - packet size =" + size());

        return mByteVector.get(at);
    }

    /**
     * Return sequence of bytes from a specific position in the message.
     * @param at position of first byte
     * @param length number of bytes
     * @exception EncodingException incorrect position or length
     * @return the number of bytes of message
     */
    public Byte[] getBytes(int at, int length) throws EncodingException {
        if(at+length-1 >= size())
            throw new EncodingException("Getting " + length + " bytes from position = " + at + " - packet size =" + size());

        Vector<Byte> subVector = new Vector<Byte>();
        for(int i = at; i < at+length; i++) {
            subVector.add(mByteVector.get(i));
        }

        return subVector.toArray(new Byte[length]);
    }

    /**
     * Representation of byte packet in string format ("x,x,x,x,x,x").
     * @return byte packet in string format
     */
    @Override
    public String toString() {
        StringBuilder packetStr = new StringBuilder();

        for (int i = 0; i < mByteVector.size(); i++) {
            packetStr.append(String.format("%d%s", mByteVector.get(i), (i < mByteVector.size() - 1) ? "," : ""));
        }

        return packetStr.toString();
    }

//======================================================================================================================
// SET
//======================================================================================================================

    /**
     * Clear byte packet and set byte array as its content.
     * @param bytesArr object-byte array
     * @return own byte packet instance
     */
    public BytePacket set(Byte[] bytesArr) {
        mByteVector.clear();
        return append(bytesArr);
    }

    /**
     * Clear byte packet and set content of other byte packet as its content.
     * @param packet other byte packet
     * @return own byte packet instance
     */
    public BytePacket set(BytePacket packet) {
        mByteVector.clear();
        return append(packet);
    }

    /**
     * Clear byte packet, convert string into byte array and set it as its content.
     * @param str string
     * @return own byte packet instance
     */
    public BytePacket set(String str) throws EncodingException {
        mByteVector.clear();
        return appendString(str);
    }

    /**
     * Copy content of byte packet into reserve byte-vector.
     */
    public void storeData() {
        mReserveByteVector = new Vector<Byte>(mByteVector);
    }

    /**
     * Copy reserve byte-vector into general byte-vector of byte packet and clear reserve vector.
     */
    public void restoreData() {
        mByteVector = new Vector<Byte>(mReserveByteVector);
        mReserveByteVector.clear();
        mReserveByteVector = null;
    }


//======================================================================================================================
// APPEND
//======================================================================================================================

    /**
     * Add given primitive-byte array to the end of byte message.
     * @param byteArr primitive-byte array
     * @return own byte packet instance
     */
    public BytePacket append(byte[] byteArr) {
        return append(ByteUtils.bytesToObjects(byteArr));
    }

    /**
     * Add given object-byte array to the end of byte message.
     * @param byteArr object-byte array
     * @return own byte packet instance
     */
    public BytePacket append(Byte[] byteArr) {
        for(Byte b : byteArr) {
            mByteVector.add(b);
        }

        return this;
    }

    /**
     * Add content of given byte packet to the end of byte message.
     * @param packet other byte packet
     * @return own byte packet instance
     */
    public BytePacket append(BytePacket packet) {
        return append(packet.getData());
    }

    /**
     * Add object-byte to the end of byte message.
     * @param b object-byte
     * @return own byte packet instance
     */
    public BytePacket appendByte(Byte b) {
        Byte[] bytesArr = {b};
        return append(bytesArr);
    }

    /**
     * Add short number in byte-form to the end of byte message.
     * @param num short number
     * @return own byte packet instance
     */
    public BytePacket appendShort(short num) {
        return append(ByteUtils.shortToBytes(num));
    }

    /**
     * Add int number in byte-form to the end of byte message.
     * @param num int number
     * @return own byte packet instance
     */
    public BytePacket appendInt(int num) {
        return append(ByteUtils.intToBytes(num));
    }

    /**
     * Add long number in byte-form to the end of byte message.
     * @param num long number
     * @return own byte packet instance
     */
    public BytePacket appendLong(long num) {
        return append(ByteUtils.longToBytes(num));
    }

    /**
     * Add string in byte-form to the end of byte message.
     * @param str string
     * @return own byte packet instance
     */
    public BytePacket appendString(String str) {
        if(str == null)
            return this;

        return append(ByteUtils.stringToBytes(str));
    }


//======================================================================================================================
// TAKE
//======================================================================================================================

    /**
     * Take away byte from the start of byte message.
     * @exception EncodingException conversion error (insufficient bytes in the package)
     * @return taken byte
     */
    public Byte takeByte() throws EncodingException {
        if(mByteVector.size() < 1)
            throw new EncodingException("Take byte exception - the packet is empty");

        return mByteVector.remove(0);
    }

    /**
     * Take away 2 bytes from the start of byte message and covert them to short.
     * @exception EncodingException conversion error (insufficient bytes in the package)
     * @return taken short
     */
    public short takeShort() throws EncodingException {
        if(mByteVector.size() < 2)
            throw new EncodingException("Take short exception - the packet does not have 2 bytes");

        Byte[] bytes = new Byte[2];
        bytes[0] = mByteVector.remove(0);
        bytes[1] = mByteVector.remove(0);

        return ByteUtils.toShort(bytes);
    }

    /**
     * Take away 4 bytes from the start of byte message and covert them to int.
     * @exception EncodingException conversion error (insufficient bytes in the package)
     * @return taken int
     */
    public int takeInt() throws EncodingException {
        if(mByteVector.size() < 4)
            throw new EncodingException("Take int exception - the packet does not have 4 bytes");

        Byte[] bytes = new Byte[4];
        for(int i = 0; i < 4; i++) {
            bytes[i] = mByteVector.remove(0);
        }

        return ByteUtils.toInt(bytes);
    }

    /**
     * Take away 8 bytes from the start of byte message and covert them to long.
     * @exception EncodingException conversion error (insufficient bytes in the package)
     * @return taken long
     */
    public long takeLong() throws EncodingException {
        if(mByteVector.size() < 8)
            throw new EncodingException("Take long exception - the packet does not have 8 bytes");

        Byte[] bytes = new Byte[8];
        for(int i = 0; i < 8; i++) {
            bytes[i] = mByteVector.remove(0);
        }

        return ByteUtils.toLong(bytes);
    }

    /**
     * Take away a certain amount of bytes from the start of byte message and covert them to string.
     * @exception EncodingException conversion error (insufficient bytes in the package)
     * @return taken string
     */
    public String takeString(int len) throws EncodingException {
        if(len == 0)
            return null;

        if(mByteVector.size() < len)
            throw new EncodingException("Take string exception - the packet does not have " + len + " bytes");

        Byte[] bytes = new Byte[len];
        for(int i = 0; i < len; i++) {
            bytes[i] = mByteVector.remove(0);
        }

        return ByteUtils.toString(bytes);
    }

    /**
     * Take away a certain amount of bytes from the start of byte message.
     * @exception EncodingException conversion error (insufficient bytes in the package)
     * @return taken byte array
     */
    public Byte[] takeBytes(int num) throws EncodingException {
        if(mByteVector.size() < num)
            throw new EncodingException("Take bytes exception - the packet does not have " + num + " bytes");

        Byte[] bytes = new Byte[num];
        for(int i = 0; i < num; i++) {
            bytes[i] = mByteVector.remove(0);
        }

        return bytes;
    }


}
