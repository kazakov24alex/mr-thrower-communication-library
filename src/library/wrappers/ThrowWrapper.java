package library.wrappers;

import library.codes.DirectionCodes;
import library.codes.ThrowModeCodes;
import library.codes.ThrowStateCodes;
import library.objects.MessageObject;
import library.objects.ThrowFileObject;


public class ThrowWrapper extends ThrowFileObject {

    private Byte dir;
    private Byte mode;
    private Byte type;
    private Byte state;

    private long currentSize;


    public ThrowWrapper(ThrowFileObject tFile, Byte dir, Byte mode, Byte type) {
        super(tFile.getTimeID(), tFile.getFilename(), tFile.getFileSize(), tFile.getMD5());
        setAddress(tFile.getAddress());
        this.setPath(tFile.getPath());

        this.dir = dir;
        this.mode = mode;
        this.type = type;

        state = ThrowStateCodes.STATE_REQUEST;
        currentSize = 0;
    }


    public Byte getDir() {
        return dir;
    }

    public Byte getMode() {
        return mode;
    }

    public Byte getType() {
        return type;
    }

    public Byte getState() {
        return state;
    }

    public int getProgress() {
        return (int) (( (float) currentSize / (float) getFileSize()) * 100) ;
    }

    public long getCurrentSize() {
        return currentSize;
    }



    public void setCurrentSize(long currentSize) {
        this.currentSize = currentSize;
    }

    public void setState(Byte state) {
        this.state = state;

        switch (state) {
            case ThrowStateCodes.STATE_FINISHED:
                currentSize = getFileSize();
                break;
            case ThrowStateCodes.STATE_DELETED:
                currentSize = getFileSize();
                break;
        }
    }


    @Override
    public String toString() {
        String directionStr = "";
        switch (dir) {
            case DirectionCodes.INCOMING:
                directionStr = "IN";
                break;
            case DirectionCodes.OUTCOMING:
                directionStr = "OUT";
                break;
        }

        String modeStr = "";
        switch (mode) {
            case ThrowModeCodes.WIFI_NETWORK:
                modeStr = ThrowModeCodes.WIFI_NETWORK_TITLE;
                break;
            case ThrowModeCodes.WIFI_DIRECT:
                modeStr = ThrowModeCodes.WIFI_DIRECT_TITLE;
                break;
            case ThrowModeCodes.BLUETOOTH:
                modeStr = ThrowModeCodes.BLUETOOTH_TITLE;
                break;
        }

        return super.toString() + " [" + directionStr + ", " + modeStr + "]";
    }


}
