package library.wrappers;

import library.exceptions.EncodingException;


/**
 * Class-wrapper for storage MD5.
 * @author Alex Kazakov
 * @version TESTED (coverage 100%)
 */
public class MD5 implements Comparable<MD5> {

    private static final int MD5_BYTES_LEN = 16;

    /** field of a 16-byte array for storage MD5 in byte format */
    private byte[] md5;


//======================================================================================================================
// CONSTRUCTORS
//======================================================================================================================

    /**
     * The constructor of MD5 initialization from primitive byte array.
     * @param md5 - MD5 in byte format
     * @exception EncodingException - incorrect source data
     */
    public MD5(byte[] md5) throws EncodingException {
        if(md5.length != MD5_BYTES_LEN) {
            throw new EncodingException("Incorrect number of bytes: " + md5.length + " != " + MD5_BYTES_LEN);
        }

        this.md5 = md5;
    }

    /**
     * The constructor of MD5 initialization from string form.
     * @param md5string - MD5 in string form
     * @exception EncodingException - incorrect source data
     */
    public MD5(String md5string) throws EncodingException {
        setMd5(md5string);
    }


//======================================================================================================================
// GETTERS
//======================================================================================================================

    /**
     * Representation of MD5 in string format ("xxxxxxxxxxxxxxxx").
     * @return Mac address in string format
     */
    @Override
    public String toString() {
        StringBuilder md5Str = new StringBuilder();

        // formatted output of each individual part
        for (int i = 0; i < md5.length; i++) {
            md5Str.append(String.format("%02X", md5[i]));
        }

        return md5Str.toString().toLowerCase();
    }

    /**
     * Return of MD5 in byte array format.
     * @return MD5 in primitive byte array format
     */
    public byte[] getBytes() {
        return md5;
    }


    /**
     * Set of MD5 from String format.
     * @param md5String - MD5 in String format
     */
    public void setMd5(String md5String) throws EncodingException {
        try {
            String[] md5strArr = new String[16];
            for(int i = 0; i < 16; i++) {
                md5strArr[i] = "" + md5String.charAt(i*2) + md5String.charAt(i*2+1);
            }

            byte[] md5bytes = new byte[16];
            for (int i = 0; i < md5strArr.length; i++) {
                md5bytes[i] = (byte) (Integer.parseInt(md5strArr[i], 16) & 0xFF);
            }

            md5 = md5bytes;
        } catch (Exception e) {
            throw new EncodingException("Incorrect Mac address: " + md5String);
        }
    }



//======================================================================================================================
// OVERRIDES
//======================================================================================================================

    /**
     * Implementation of the comparison method of Comparable interface.
     * @param otherMD5 - comparable MD5
     */
    @Override
    public int compareTo(MD5 otherMD5) {
        // mathematical comparison of each individual part
        for(int i = 0; i < MD5_BYTES_LEN; i++) {
            if(md5[i] < otherMD5.getBytes()[i])
                return -1;
            if(md5[i] > otherMD5.getBytes()[i])
                return 1;
        }

        return 0;
    }

}
