package library.wrappers;

import library.ByteUtils;
import library.exceptions.EncodingException;

import java.io.IOException;


/**
 * Class-wrapper for storage Mac address.
 * @author Alex Kazakov
 * @version TESTED (coverage 100%)
 */
public class MacAddress implements Comparable<MacAddress> {

    /** field of a six-byte array for storage Mac address in byte format */
    private byte[] mMac;


//======================================================================================================================
// CONSTRUCTORS
//======================================================================================================================

    /**
     * The constructor of the empty Mac address.
     */
    public MacAddress() {
        mMac = null;
    }

    /**
     * The constructor of Mac address initialization from primitive byte array.
     * @param macAddress - Mac address in byte format
     * @exception EncodingException - incorrect source data
     */
    public MacAddress(byte[] macAddress) throws EncodingException {
        setMac(macAddress);
    }

    /**
     * The constructor of Mac address initialization from object Byte array.
     * @param macAddress - Mac address in Byte format
     * @exception EncodingException -  incorrect source data
     */
    public MacAddress(Byte[] macAddress) throws EncodingException {
        setMac(ByteUtils.toPrimitives(macAddress));
    }

    /**
     * The constructor of Mac address initialization from String.
     * @param macAddress - Mac address in String format
     * @exception EncodingException -  incorrect source data
     */
    public MacAddress(String macAddress) throws EncodingException {
        setMac(macAddress);
    }


//======================================================================================================================
// GETTERS
//======================================================================================================================

    public static MacAddress getFromSolidFormat(String macSolidStr) throws EncodingException {
        try {
            String[] macStrArr = new String[6];
            for(int i = 0; i < 6; i++) {
                macStrArr[i] = "" + macSolidStr.charAt(i*2) + macSolidStr.charAt(i*2+1);
            }

            byte[] macBytes = new byte[6];
            for (int i = 0; i < macStrArr.length; i++) {
                macBytes[i] = (byte) (Integer.parseInt(macStrArr[i], 16) & 0xFF);
            }

            return new MacAddress(macBytes);
        } catch (Exception e) {
            throw new EncodingException("Incorrect Mac address: " + macSolidStr);
        }
    }

    /**
     * Representation of Mac address in string format ("xx-xx-xx-xx-xx-xx").
     * @return Mac address in string format
     */
    @Override
    public String toString() {
        StringBuilder macStr = new StringBuilder();

        // check for empty
        if(mMac == null)
            return null;

        // formatted output of each individual part
        for (int i = 0; i < mMac.length; i++) {
            macStr.append(String.format("%02X%s", mMac[i], (i < mMac.length - 1) ? ":" : ""));
        }

        return macStr.toString();
    }

    /**
     * Return of Mac address in byte array format.
     * @return Mac address in primitive byte array format
     */
    public byte[] getBytes() {
        return mMac;
    }


//======================================================================================================================
// SETTERS
//======================================================================================================================

    /**
     * Set of Mac address from primitive byte array format.
     * @param macAddress - Mac address in byte format
     */
    public void setMac(byte[] macAddress) throws EncodingException {
        // check Mac address correctness
        if(macAddress.length != 6)
            throw new EncodingException("Incorrect number of  part of Mac address: " + macAddress.length);

        mMac = new byte[6];

        System.arraycopy(macAddress, 0, mMac, 0, 6);
    }

    /**
     * Set of Mac address from String format.
     * @param macAddressString - Mac address in String format
     */
    public void setMac(String macAddressString) throws EncodingException {
        try {
            String[] macArr = macAddressString.split(":");

            if(macArr.length != 6) {
                throw new EncodingException("Incorrect Mac address: " + macAddressString);
            }

            byte[] macAddress = new byte[6];
            for (int i = 0; i < macArr.length; i++) {
                macAddress[i] = (byte) (Integer.parseInt(macArr[i], 16) & 0xFF);
            }

            setMac(macAddress);
        } catch (Exception e) {
            throw new EncodingException("Incorrect Mac address: " + macAddressString);
        }
    }


//======================================================================================================================
// OVERRIDES
//======================================================================================================================

    /**
     * Implementation of the comparison method of Comparable interface.
     * @param otherMac - comparable Mac address
     */
    @Override
    public int compareTo(MacAddress otherMac) {
        // mathematical comparison of each individual part
        for(int i = 0; i < 6; i++) {
            if(mMac[i] < otherMac.getBytes()[i])
                return -1;
            if(mMac[i] > otherMac.getBytes()[i])
                return 1;
        }

        return 0;
    }


}
