package library.wrappers;

import library.ByteUtils;
import library.exceptions.EncodingException;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Class-wrapper for storage IPv4 address.
 * @author Alex Kazakov
 * @version TESTED (coverage 100%)
 */
public class IpAddress implements Comparable<IpAddress> {

    /** field of a four-byte array for storage IPv4 address in byte format */
    private byte[] mIp;


//======================================================================================================================
// CONSTRUCTORS
//======================================================================================================================

    /**
     * The constructor of the empty IPv4 address.
     */
    public IpAddress() {
        mIp = null;
    }

    /**
     * The constructor of IPv4 address initialization from primitive byte array.
     * @param ipAddress - IPv4 address in byte format
     * @exception EncodingException - exclusion of incorrect source data
     */
    public IpAddress(byte[] ipAddress) throws EncodingException {
        setIp(ipAddress);
    }

    /**
     * The constructor of IPv4 address initialization from object Byte array.
     * @param ipAddress - IPv4 address in Byte format
     * @exception EncodingException - exclusion of incorrect source data
     */
    public IpAddress(Byte[] ipAddress) throws EncodingException {
        setIp(ByteUtils.toPrimitives(ipAddress));
    }

    /**
     * The constructor of IPv4 address initialization from primitive short array.
     * @param ipAddress - IPv4 address in short format
     * @exception EncodingException - exclusion of incorrect source data
     */
    public IpAddress(short[] ipAddress) throws EncodingException {
        mIp = new byte[4];
        setIp(ipAddress);
    }

    /**
     * The constructor of IPv4 address initialization from String.
     * @param ipAddress - IPv4 address in String format
     * @exception EncodingException - exclusion of incorrect source data
     */
    public IpAddress(String ipAddress) throws EncodingException {
        setIp(ipAddress);
    }


//======================================================================================================================
// GETTERS
//======================================================================================================================

    /**
     * Representation of IPv4 address in string format ("x.x.x.x").
     * @return IPv4 address in string format
     */
    @Override
    public String toString() {
        if(mIp == null)
            return "empty";

        StringBuilder ipStr = new StringBuilder();

        // convert IP Address to perceptible view
        short[] ipShorts = getShorts();

        // formatted output of each individual part
        for (int i = 0; i < ipShorts.length; i++) {
            ipStr.append(String.format("%d%s", ipShorts[i], (i < ipShorts.length - 1) ? "." : ""));
        }

        return ipStr.toString();
    }

    /**
     * Return of IPv4 address in byte array format.
     * @return IPv4 address in primitive byte array format
     */
    public byte[] getBytes() {
        return mIp;
    }

    /**
     * Return of IPv4 address in short array format.
     * @return IPv4 address in primitive short array format
     */
    public short[] getShorts() {
        short[] shorts = new short[4];

        // convert from bytes to shorts format
        for(int i = 0; i < 4; i++) {
            if (mIp[i] < 0) {
                shorts[i] = (short) (mIp[i] + 256);
            } else {
                shorts[i] = (short) mIp[i];
            }
        }

        return shorts;
    }


//======================================================================================================================
// SETTERS
//======================================================================================================================

    /**
     * Set of IPv4 address from primitive byte array format.
     * @param ipAddress - IPv4 address in byte format
     */
    public void setIp(byte[] ipAddress) throws EncodingException {
        // check IP address correctness
        if(ipAddress.length != 4)
            throw new EncodingException("Incorrect number of  part of IP address: " + ipAddress.length);

        mIp = new byte[4];

        System.arraycopy (ipAddress, 0, mIp, 0, 4);
    }

    /**
     * Set of IPv4 address from primitive short array format.
     * @param ipAddress - IPv4 address in short format
     */
    public void setIp(short[] ipAddress) throws EncodingException {
        // check IP address correctness
        if(ipAddress.length != 4)
            throw new EncodingException("Incorrect number of  part of IP address: " + ipAddress.length);

        mIp = new byte[4];

        for(int i = 0; i < 4; i++) {
            // check IP address correctness
            if(ipAddress[i] > 255 || ipAddress[i] < 0) {
                throw new EncodingException("Incorrect " + i + " part of IP address: " +ipAddress[i]);
            }

            // convert from short to byte format
            if (ipAddress[i] > 127) {
                mIp[i] = (byte) (ipAddress[i] - 256);
            } else {
                mIp[i] = (byte) ipAddress[i];
            }
        }
    }


    /**
     * Set of IPv4 address from String.
     * @param ipString - IPv4 address in short format
     */
    public void setIp(String ipString) throws EncodingException {
        try {
            String[] ipArr = ipString.split("\\.");

            if(ipArr.length != 4) {
                throw new EncodingException("Incorrect Mac address: " + ipString);
            }

            short[] ipAddressShorts = new short[4];
            for (int i = 0; i < ipArr.length; i++) {
                ipAddressShorts[i] = Short.parseShort(ipArr[i]);
            }

            setIp(ipAddressShorts);
        } catch (Exception e) {
            throw new EncodingException("Incorrect Mac address: " + ipString);
        }

    }

//======================================================================================================================
// COMPARABLE
//======================================================================================================================

    /**
     * Implementation of the comparison method of Comparable interface.
     * @param otherIp - comparable IPv4 address
     */
    @Override
    public int compareTo(IpAddress otherIp) {
        // convert IP Address to perceptible view
        short[] ipShort = getShorts();
        short[] otherIpShort = otherIp.getShorts();

        // mathematical comparison of each individual part
        for(int i = 0; i < 4; i++) {
            if(ipShort[i] < otherIpShort[i])
                return -1;
            if(ipShort[i] > otherIpShort[i])
                return 1;
        }

        return 0;
    }


}
