package library.wrappers;


import library.objects.BaseProtocolObject;

public class EventWrapper {

//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    public static final Byte TO_EVENT_MANAGER   = 1;
    public static final Byte FROM_EVENT_MANAGER = 2;
    public static final Byte TO_THROW_MANAGER   = 3;


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private BaseProtocolObject protocolObj;
    private Byte direction;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public EventWrapper(BaseProtocolObject protocolObj, Byte direction) {
        this.protocolObj = protocolObj;
        this.direction = direction;
    }


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public BaseProtocolObject getObject() {
        return protocolObj;
    }

    public Byte getCode() {
        return protocolObj.getCode();
    }

    public Byte getDirection() {
        return direction;
    }


}
