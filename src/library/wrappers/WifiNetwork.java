package library.wrappers;

/**
 * Class-wrapper for storage WiFi network.
 * @author Alex Kazakov
 * @version TESTED (coverage 100%)
 */
public class WifiNetwork {

    /** field of a SSID of WiFi network */
    private String ssid;
    /** field of a Wifi network type */
    private String networkType;
    /** field of a authentication type of WiFi network */
    private String authentication;
    /** field of a encryption type of WiFi network */
    private String encryption;


//======================================================================================================================
// CONSTRUCTORS
//======================================================================================================================

    /**
     * The constructor of the WiFi network.
     */
    public WifiNetwork(String ssid, String networkType, String authentication, String encryption) {
        this.ssid = ssid;
        this.networkType = networkType;
        this.authentication = authentication;
        this.encryption = encryption;
    }


//======================================================================================================================
// GETTERS
//======================================================================================================================

    /**
     * Representation of WiFi network properties in string format.
     * @return WiFi network properties in string format
     */
    @Override
    public String toString() {
        return "\nSSID: " + ssid + "\n" +
                "\tNetwork type:\t" + networkType + "\n" +
                "\tAuthentication:\t" + authentication + "\n" +
                "\tEncryption:\t\t" + encryption;
    }

    /**
     * Is there a password on the WiFi network.
     * @return boolean - is there a password on the WiFi network
     */
    public boolean isPasswordProtected() {
        if(authentication.contains("WPA"))
            return true;
        else
            return false;
    }

    /**
     * Return SSID property of WiFi network.
     * @return SSID property of WiFi network
     */
    public String getSsid() {
        return ssid;
    }

    /**
     * Return Network Type property of WiFi network.
     * @return Network Type property of WiFi network
     */
    public String getNetworkType() {
        return networkType;
    }

    /**
     * Return Authentication property of WiFi network.
     * @return Authentication property of WiFi network
     */
    public String getAuthentication() {
        return authentication;
    }

    /**
     * Return Encryption property of WiFi network.
     * @return Encryption property of WiFi network
     */
    public String getEncryption() {
        return encryption;
    }


}