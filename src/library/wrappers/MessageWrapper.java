package library.wrappers;

import library.codes.DirectionCodes;
import library.codes.ThrowModeCodes;
import library.codes.ThrowStateCodes;
import library.objects.MessageObject;

import java.util.Date;


public class MessageWrapper extends MessageObject {

    private Byte dir;
    private Byte mode;
    private Byte state;


    public MessageWrapper(MessageObject message, Byte dir, Byte mode) {
        super(message.getTimeID(), message.getText());
        setAddress(message.getAddress());

        this.dir = dir;
        this.mode = mode;

        state = ThrowStateCodes.STATE_REQUEST;
    }


    public Byte getState() {
        return state;
    }

    public Byte getDir() {
        return dir;
    }

    public Byte getMode() {
        return mode;
    }


    public void setState(Byte state) {
        this.state = state;
    }

    @Override
    public String toString() {
        String directionStr = "";
        switch (dir) {
            case DirectionCodes.INCOMING:
                directionStr = "IN";
                break;
            case DirectionCodes.OUTCOMING:
                directionStr = "OUT";
                break;
        }

        String modeStr = "";
        switch (mode) {
            case ThrowModeCodes.WIFI_NETWORK:
                modeStr = ThrowModeCodes.WIFI_NETWORK_TITLE;
                break;
            case ThrowModeCodes.WIFI_DIRECT:
                modeStr = ThrowModeCodes.WIFI_DIRECT_TITLE;
                break;
            case ThrowModeCodes.BLUETOOTH:
                modeStr = ThrowModeCodes.BLUETOOTH_TITLE;
                break;
        }

        return super.toString() + " [" + directionStr + ", " + modeStr + "]";
    }


}
