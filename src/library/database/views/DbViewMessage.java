package library.database.views;

import library.database.RecordReport;
import library.database.tables.*;
import library.exceptions.EncodingException;
import library.exceptions.ParsePacketException;
import library.objects.GreetingObject;
import library.objects.MessageObject;
import library.wrappers.IpAddress;
import library.wrappers.MacAddress;
import library.wrappers.MessageWrapper;

import java.util.Date;


public class DbViewMessage {

// =================================================================================================
// CONTRACT
// =================================================================================================
    
    public static final String VIEW_NAME = "message_view";

    public static final String COL_ID        = "id";
    public static final String COL_MAC_ID    = "mac_id";
    public static final String COL_MAC       = "mac";
    public static final String COL_MODE_ID   = "mode_id";
    public static final String COL_MODE      = "mode";
    public static final String COL_STATE_ID  = "state_id";  // 5
    public static final String COL_STATE     = "state";
    public static final String COL_DIR_ID    = "dir_id";
    public static final String COL_DIR       = "dir";
    public static final String COL_TIME_ID   = "time_id";

    public static final String COL_TEXT      = "text";      // 10

// =================================================================================================
// QUERIES
// =================================================================================================

// CREATE
    public static String createTableQuery() {
        return "CREATE VIEW " + VIEW_NAME + " AS SELECT " +
                DbTableMessage.TABLE_NAME + "." + DbTableMessage.COL_ID + " AS " + COL_ID + "," +
                DbTableMessage.TABLE_NAME + "." + DbTableMessage.COL_CONTACT_ID + " AS " + COL_MAC_ID + "," +
                DbTableContact.TABLE_NAME + "." + DbTableContact.COL_MAC + " AS " + COL_MAC + "," +
                DbTableMessage.TABLE_NAME + "." + DbTableMessage.COL_MODE_ID + " AS " + COL_MODE_ID + "," +
                DbTableThrowMode.TABLE_NAME + "." + DbTableThrowMode.COL_MODE + " AS " + COL_MODE + "," +
                DbTableMessage.TABLE_NAME + "." + DbTableMessage.COL_STATE_ID + " AS " + COL_STATE_ID + "," +
                DbTableThrowState.TABLE_NAME + "." + DbTableThrowState.COL_STATE + " AS " + COL_STATE + "," +
                DbTableMessage.TABLE_NAME + "." + DbTableMessage.COL_DIR_ID + " AS " + COL_DIR_ID + "," +
                DbTableDirection.TABLE_NAME + "." + DbTableDirection.COL_DIRECTION + " AS " + COL_DIR + "," +
                DbTableMessage.TABLE_NAME + "." + DbTableMessage.COL_TIME_ID + " AS " + COL_TIME_ID + "," +
                DbTableMessage.TABLE_NAME + "." + DbTableMessage.COL_TEXT + " AS " + COL_TEXT +
                " FROM " + DbTableMessage.TABLE_NAME + " " + "INNER JOIN " + DbTableContact.TABLE_NAME + "," + DbTableThrowMode.TABLE_NAME + "," + DbTableThrowState.TABLE_NAME + "," + DbTableDirection.TABLE_NAME +
                " ON " + DbTableMessage.TABLE_NAME + "." + DbTableMessage.COL_CONTACT_ID + "=" + DbTableContact.TABLE_NAME + "." + DbTableContact.COL_ID + " " +
                " AND " + DbTableMessage.TABLE_NAME + "." + DbTableMessage.COL_MODE_ID + "=" + DbTableThrowMode.TABLE_NAME + "." + DbTableThrowMode.COL_ID + " " +
                " AND " + DbTableMessage.TABLE_NAME + "." + DbTableMessage.COL_STATE_ID + "=" + DbTableThrowState.TABLE_NAME + "." + DbTableThrowState.COL_ID + " " +
                " AND " + DbTableMessage.TABLE_NAME + "." + DbTableMessage.COL_DIR_ID + "=" + DbTableDirection.TABLE_NAME + "." + DbTableDirection.COL_ID;

    }

// SELECT
    private static String selectQuery(String whereClause) {
        return "SELECT * FROM " + VIEW_NAME + " " + whereClause + ";";
    }

    public static String selectByMac(MacAddress macAddress) {
        String whereClause = "WHERE " + COL_MAC + " == " + "'" + macAddress.toString() + "'";
        return selectQuery(whereClause);
    }


// =================================================================================================
// DECODING
// =================================================================================================

    public static MessageWrapper decodeRecordReport(RecordReport report) throws ParsePacketException, EncodingException {
        MessageObject message = new MessageObject(
                new Date(Long.parseLong(report.getField(9))),
                report.getField(10)
        );
        message.setAddress(new MacAddress(report.getField(2)));

        return new MessageWrapper(
                message,
                Byte.parseByte(report.getField(7)),
                Byte.parseByte(report.getField(3))
        );
    }


}
