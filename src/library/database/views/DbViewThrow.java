package library.database.views;

import library.database.RecordReport;
import library.database.tables.*;
import library.exceptions.EncodingException;
import library.exceptions.ParsePacketException;
import library.objects.ThrowFileObject;
import library.wrappers.MD5;
import library.wrappers.MacAddress;
import library.wrappers.ThrowWrapper;

import java.util.Date;


public class DbViewThrow {

// =================================================================================================
// CONTRACT
// =================================================================================================

    public static final String VIEW_NAME = "throw_view";

    public static final String COL_ID        = "id";
    public static final String COL_MAC_ID    = "mac_id";
    public static final String COL_MAC       = "mac";
    public static final String COL_MODE_ID   = "mode_id";
    public static final String COL_MODE      = "mode";
    public static final String COL_TYPE_ID   = "type_id";   // 5
    public static final String COL_TYPE      = "type";
    public static final String COL_STATE_ID  = "state_id";
    public static final String COL_STATE     = "state";
    public static final String COL_DIR_ID    = "dir_id";
    public static final String COL_DIR       = "dir";       // 10
    public static final String COL_TIME_ID   = "time_id";

    public static final String COL_FILENAME  = "filename";
    public static final String COL_SIZE      = "size";
    public static final String COL_MD5       = "md5";
    public static final String COL_PATH      = "path";


// =================================================================================================
// QUERIES
// =================================================================================================

// CREATE
    public static String createTableQuery() {
        return "CREATE VIEW " + VIEW_NAME + " AS SELECT " +
                DbTableThrow.TABLE_NAME + "." + DbTableThrow.COL_ID + " AS " + COL_ID + "," +
                DbTableThrow.TABLE_NAME + "." + DbTableThrow.COL_CONTACT_ID + " AS " + COL_MAC_ID + "," +
                DbTableContact.TABLE_NAME + "." + DbTableContact.COL_MAC + " AS " + COL_MAC + "," +
                DbTableThrow.TABLE_NAME + "." + DbTableThrow.COL_MODE_ID + " AS " + COL_MODE_ID + "," +
                DbTableThrowMode.TABLE_NAME + "." + DbTableThrowMode.COL_MODE + " AS " + COL_MODE + "," +
                DbTableThrow.TABLE_NAME + "." + DbTableThrow.COL_TYPE_ID + " AS " + COL_TYPE_ID + "," +
                DbTableThrowType.TABLE_NAME + "." + DbTableThrowType.COL_TYPE + " AS " + COL_TYPE + "," +
                DbTableThrow.TABLE_NAME + "." + DbTableThrow.COL_STATE_ID + " AS " + COL_STATE_ID + "," +
                DbTableThrowState.TABLE_NAME + "." + DbTableThrowState.COL_STATE + " AS " + COL_STATE + "," +
                DbTableThrow.TABLE_NAME + "." + DbTableThrow.COL_DIR_ID + " AS " + COL_DIR_ID + "," +
                DbTableDirection.TABLE_NAME + "." + DbTableDirection.COL_DIRECTION + " AS " + COL_DIR + "," +
                DbTableThrow.TABLE_NAME + "." + DbTableThrow.COL_TIME_ID + " AS " + COL_TIME_ID + "," +
                DbTableThrow.TABLE_NAME + "." + DbTableThrow.COL_FILENAME + " AS " + COL_FILENAME + "," +
                DbTableThrow.TABLE_NAME + "." + DbTableThrow.COL_SIZE + " AS " + COL_SIZE + "," +
                DbTableThrow.TABLE_NAME + "." + DbTableThrow.COL_MD5 + " AS " + COL_MD5 + "," +
                DbTableThrow.TABLE_NAME + "." + DbTableThrow.COL_PATH + " AS " + COL_PATH +
                " FROM " + DbTableThrow.TABLE_NAME + " " + "INNER JOIN " + DbTableContact.TABLE_NAME + "," + DbTableThrowMode.TABLE_NAME + "," + DbTableThrowType.TABLE_NAME + "," + DbTableThrowState.TABLE_NAME + "," + DbTableDirection.TABLE_NAME +
                " ON " + DbTableThrow.TABLE_NAME + "." + DbTableThrow.COL_CONTACT_ID + "=" + DbTableContact.TABLE_NAME + "." + DbTableContact.COL_ID + " " +
                " AND " + DbTableThrow.TABLE_NAME + "." + DbTableThrow.COL_MODE_ID + "=" + DbTableThrowMode.TABLE_NAME + "." + DbTableThrowMode.COL_ID + " " +
                " AND " + DbTableThrow.TABLE_NAME + "." + DbTableThrow.COL_TYPE_ID + "=" + DbTableThrowType.TABLE_NAME + "." + DbTableThrowType.COL_ID + " " +
                " AND " + DbTableThrow.TABLE_NAME + "." + DbTableThrow.COL_STATE_ID + "=" + DbTableThrowState.TABLE_NAME + "." + DbTableThrowState.COL_ID + " " +
                " AND " + DbTableThrow.TABLE_NAME + "." + DbTableThrow.COL_DIR_ID + "=" + DbTableDirection.TABLE_NAME + "." + DbTableDirection.COL_ID;
    }

// SELECT
    private static String selectQuery(String whereClause) {
        return "SELECT * FROM " + VIEW_NAME + " " + whereClause + ";";
    }

    public static String selectByMac(MacAddress macAddress) {
        String whereClause = "WHERE " + COL_MAC + " == " + "'" + macAddress.toString() + "'";
        return selectQuery(whereClause);
    }


// =================================================================================================
// DECODING
// =================================================================================================

    public static ThrowWrapper decodeRecordReport(RecordReport report) throws ParsePacketException, EncodingException {
        ThrowFileObject tFile = new ThrowFileObject(
                new Date(Long.parseLong(report.getField(11))),
                report.getField(12),
                Long.parseLong(report.getField(13)),
                new MD5(report.getField(14))
        );
        tFile.setAddress(new MacAddress(report.getField(2)));

        ThrowWrapper throwWr = new ThrowWrapper(
                tFile,
                Byte.parseByte(report.getField(9)),
                Byte.parseByte(report.getField(3)),
                Byte.parseByte(report.getField(5))
        );
        throwWr.setState(Byte.parseByte(report.getField(7)));
        throwWr.setPath(report.getField(15));

        return throwWr;
    }


}