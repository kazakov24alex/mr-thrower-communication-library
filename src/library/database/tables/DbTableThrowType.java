package library.database.tables;

import library.codes.ThrowTypeCodes;

import java.util.ArrayList;


public class DbTableThrowType {

// =================================================================================================
// CONTRACT
// =================================================================================================

    public static final String TABLE_NAME = "types";

    public static final String COL_ID    = "id";
    public static final String COL_TYPE  = "throw_type";

    public static final String DOMAIN_ID         = " INTEGER PRIMARY KEY NOT NULL";
    public static final String DOMAIN_THROW_TYPE = " TEXT NOT NULL";

// =================================================================================================
// QUERIES
// =================================================================================================

// CREATE
    public static String createTableQuery() {
        return "CREATE TABLE " + TABLE_NAME + " (" +
                COL_ID + DOMAIN_ID + "," +
                COL_TYPE + DOMAIN_THROW_TYPE + ");";
    }

// INSERT
    private static String insertQuery(byte typeId, String typeTitle) {
        return "INSERT INTO " + TABLE_NAME + " VALUES ("+typeId+", '"+typeTitle +"');";
    }

// FILL
    public static ArrayList<String> fillQueries() {
        ArrayList<String> queries = new ArrayList<>();

        queries.add(insertQuery(ThrowTypeCodes.FILE, ThrowTypeCodes.FILE_TITLE));
        queries.add(insertQuery(ThrowTypeCodes.FOLDER, ThrowTypeCodes.FOLDER_TITLE));

        return queries;
    }


}