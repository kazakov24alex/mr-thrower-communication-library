package library.database.tables;


import library.database.Database;
import library.wrappers.MacAddress;
import library.wrappers.MessageWrapper;

import java.util.Date;

public class DbTableMessage {

// =================================================================================================
// CONTRACT
// =================================================================================================

    public static final String TABLE_NAME = "messages";

    public static final String COL_ID        = "id";
    public static final String COL_CONTACT_ID= "contact_id";
    public static final String COL_MODE_ID   = "mode_id";
    public static final String COL_STATE_ID  = "state_id";
    public static final String COL_DIR_ID    = "direction_id";
    public static final String COL_TIME_ID   = "time_id";
    public static final String COL_TEXT      = "text";

    public static final String DOMAIN_ID         = " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL";
    public static final String DOMAIN_CONTACT    = " INTEGER NOT NULL";
    public static final String DOMAIN_MODE       = " INTEGER NOT NULL";
    public static final String DOMAIN_STATUS     = " INTEGER NOT NULL";
    public static final String DOMAIN_DIRECTION  = " INTEGER NOT NULL";
    public static final String DOMAIN_TIME_ID    = " INTEGER NOT NULL";
    public static final String DOMAIN_TEXT       = " TEXT NOT NULL";

    public static final String FK_CONTACT    = "FOREIGN KEY ("+ COL_CONTACT_ID+") REFERENCES "+DbTableContact.TABLE_NAME+" ("+DbTableContact.COL_ID+")";
    public static final String FK_MODE       = "FOREIGN KEY ("+ COL_MODE_ID+") REFERENCES "+DbTableThrowMode.TABLE_NAME+" ("+DbTableThrowMode.COL_ID+")";
    public static final String FK_STATUS     = "FOREIGN KEY ("+ COL_STATE_ID+") REFERENCES "+DbTableThrowState.TABLE_NAME+" ("+DbTableThrowState.COL_ID+")";
    public static final String FK_DIRECTION  = "FOREIGN KEY ("+ COL_DIR_ID+") REFERENCES "+DbTableDirection.TABLE_NAME+" ("+DbTableDirection.COL_ID+")";


// =================================================================================================
// QUERIES
// =================================================================================================

// CREATE
    public static String createTableQuery() {
        return "CREATE TABLE " + TABLE_NAME + " (" +
                COL_ID + DOMAIN_ID + "," +
                COL_CONTACT_ID + DOMAIN_CONTACT + "," +
                COL_MODE_ID + DOMAIN_MODE + "," +
                COL_STATE_ID + DOMAIN_STATUS + "," +
                COL_DIR_ID + DOMAIN_DIRECTION + "," +
                COL_TIME_ID + DOMAIN_TIME_ID + "," +
                COL_TEXT + DOMAIN_TEXT + "," +
                FK_CONTACT  + "," +
                FK_MODE + "," +
                FK_STATUS + "," +
                FK_DIRECTION + ");";
    }

// INSERT
    public static String insertQuery(int contactId, MessageWrapper message) {
        return "INSERT INTO " + TABLE_NAME + " VALUES (null, " +
                contactId + ", " + message.getMode() + ", " + message.getState() + ", " + message.getDir() + ", " +
                message.getTimeID().getTime() + ", '" + message.getText() + "');";
    }

// UPDATE
    public static String updateStateQuery(int contactId, Date timeID, Byte state) {
        return "UPDATE " + TABLE_NAME + " SET " + COL_STATE_ID + " = " + state +
                " WHERE " + COL_CONTACT_ID + " == " + contactId + " AND " + COL_TIME_ID + " == " + timeID.getTime() + ";";
    }

// DELETE
    private static String deleteQuery(String whereClause) {
        return "DELETE FROM " + TABLE_NAME + " " + whereClause + ";";
    }

    public static String deleteAllQuery() {
        return deleteQuery("");
    }

    public static String deleteByContactIdQuery(int contactId) {
        String whereClause = "WHERE " + COL_CONTACT_ID + " == " + contactId;
        return deleteQuery(whereClause);
    }

    public static String deleteByTimeIdQuery(int contactId, Date timeId) {
        String whereClause = "WHERE " + COL_CONTACT_ID + " == " + contactId + " AND " + COL_TIME_ID + " == " + timeId.getTime();
        return deleteQuery(whereClause);
    }


}
