package library.database.tables;


import library.codes.ThrowModeCodes;
import library.codes.ThrowStateCodes;

import java.util.ArrayList;

public class DbTableThrowState {

// =================================================================================================
// CONTRACT
// =================================================================================================

    public static final String TABLE_NAME = "states";

    public static final String COL_ID    = "id";
    public static final String COL_STATE = "state";

    public static final String DOMAIN_ID      = " INTEGER PRIMARY KEY NOT NULL";
    public static final String DOMAIN_STATE  = " TEXT NOT NULL";


// =================================================================================================
// QUERIES
// =================================================================================================

// CREATE
    public static String createTableQuery() {
        return "CREATE TABLE " + TABLE_NAME + " (" +
                COL_ID + DOMAIN_ID + "," +
                COL_STATE + DOMAIN_STATE + ");";
    }

// INSERT
    private static String insertQuery(byte stateId, String stateTitle) {
        return "INSERT INTO " + TABLE_NAME + " VALUES ("+stateId+", '"+stateTitle +"');";
    }

// FILL
    public static ArrayList<String> fillQueries() {
        ArrayList<String> queries = new ArrayList<>();

        queries.add(insertQuery(ThrowStateCodes.STATE_REQUEST, ThrowStateCodes.STATE_REQUEST_TITLE));
        queries.add(insertQuery(ThrowStateCodes.STATE_REJECTED, ThrowStateCodes.STATE_REJECTED_TITLE));
        queries.add(insertQuery(ThrowStateCodes.STATE_PROCESS, ThrowStateCodes.STATE_PROCESS_TITLE));
        queries.add(insertQuery(ThrowStateCodes.STATE_INTERRUPTED, ThrowStateCodes.STATE_INTERRUPTED_TITLE));
        queries.add(insertQuery(ThrowStateCodes.STATE_FINISHED, ThrowStateCodes.STATE_FINISHED_TITLE));
        queries.add(insertQuery(ThrowStateCodes.STATE_DELETED, ThrowStateCodes.STATE_DELETED_TITLE));

        return queries;
    }


}