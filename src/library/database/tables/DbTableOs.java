package library.database.tables;

import library.codes.OsCodes;

import java.util.ArrayList;

public class DbTableOs {

// =================================================================================================
// CONTRACT
// =================================================================================================

    public static final String TABLE_NAME = "oses";

    public static final String COL_ID    = "id";
    public static final String COL_OS    = "os";

    public static final String DOMAIN_ID = " INTEGER PRIMARY KEY NOT NULL";
    public static final String DOMAIN_OS = " TEXT NOT NULL";


// =================================================================================================
// QUERIES
// =================================================================================================

// CREATE
    public static String createTableQuery() {
        return "CREATE TABLE " + TABLE_NAME + " (" +
                        COL_ID + DOMAIN_ID + "," +
                        COL_OS + DOMAIN_OS + ");";
    }

// INSERT
    private static String insertQuery(byte osId, String osTitle) {
        return "INSERT INTO " + TABLE_NAME + " VALUES ("+osId+", '"+osTitle +"');";
    }

// FILL
    public static ArrayList<String> fillQueries() {
        ArrayList<String> queries = new ArrayList<>();

        queries.add(insertQuery(OsCodes.WINDOWS, OsCodes.WINDOWS_TITLE));
        queries.add(insertQuery(OsCodes.ANDROID, OsCodes.ANDROID_TITLE));

        return queries;
    }


}
