package library.database.tables;


import library.codes.DirectionCodes;
import library.codes.ThrowStateCodes;

import java.util.ArrayList;

public class DbTableDirection {

// =================================================================================================
// CONTRACT
// =================================================================================================

    public static final String TABLE_NAME = "directions";

    public static final String COL_ID        = "id";
    public static final String COL_DIRECTION = "dir";

    public static final String DOMAIN_ID         = " INTEGER PRIMARY KEY NOT NULL";
    public static final String DOMAIN_DIRECTION  = " TEXT NOT NULL";


// =================================================================================================
// QUERIES
// =================================================================================================

// CREATE
    public static String createTableQuery() {
        return "CREATE TABLE " + TABLE_NAME + " (" +
                COL_ID + DOMAIN_ID + "," +
                COL_DIRECTION + DOMAIN_DIRECTION + ");";
    }

// INSERT
    public static String insertQuery(byte dirId, String dirTitle) {
        return "INSERT INTO " + TABLE_NAME + " VALUES ("+dirId+", '"+dirTitle +"');";
    }

// FILL
    public static ArrayList<String> fillQueries() {
        ArrayList<String> queries = new ArrayList<>();

        queries.add(insertQuery(DirectionCodes.INCOMING, DirectionCodes.INCOMING_TITLE));
        queries.add(insertQuery(DirectionCodes.OUTCOMING, DirectionCodes.OUTCOMING_TITLE));

        return queries;
    }


}