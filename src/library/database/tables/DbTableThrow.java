package library.database.tables;


import library.wrappers.MD5;
import library.wrappers.ThrowWrapper;

import java.util.Date;

public class DbTableThrow {

// =================================================================================================
// CONTRACT
// =================================================================================================
    
    public static final String TABLE_NAME = "throws";

    public static final String COL_ID            = "id";
    public static final String COL_CONTACT_ID    = "contact_id";
    public static final String COL_MODE_ID       = "mode";
    public static final String COL_TYPE_ID       = "type";
    public static final String COL_STATE_ID      = "state";
    public static final String COL_DIR_ID        = "direction";
    public static final String COL_TIME_ID       = "time_id";
    public static final String COL_FILENAME      = "filename";
    public static final String COL_SIZE          = "size";
    public static final String COL_MD5           = "md5";
    public static final String COL_PATH          = "path";

    public static final String DOMAIN_ID         = " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL";
    public static final String DOMAIN_CONTACT    = " INTEGER NOT NULL";
    public static final String DOMAIN_MODE       = " INTEGER NOT NULL";
    public static final String DOMAIN_FILE_TYPE  = " INTEGER NOT NULL";
    public static final String DOMAIN_STATE      = " INTEGER NOT NULL";
    public static final String DOMAIN_DIRECTION  = " INTEGER NOT NULL";
    public static final String DOMAIN_TIME_ID    = " INTEGER NOT NULL";
    public static final String DOMAIN_FILENAME   = " TEXT NOT NULL";
    public static final String DOMAIN_SIZE       = " INTEGER NOT NULL";
    public static final String DOMAIN_MD5        = " BLOB NOT NULL";
    public static final String DOMAIN_PATH       = " TEXT";

    public static final String FK_CONTACT    = "FOREIGN KEY ("+ COL_CONTACT_ID +") REFERENCES "+DbTableContact.TABLE_NAME+" ("+DbTableContact.COL_ID+")";
    public static final String FK_FILE_TYPE  = "FOREIGN KEY ("+ COL_TYPE_ID +") REFERENCES "+DbTableThrowType.TABLE_NAME+" ("+DbTableThrowType.COL_ID+")";
    public static final String FK_MODE       = "FOREIGN KEY ("+ COL_MODE_ID +") REFERENCES "+ DbTableThrowMode.TABLE_NAME+" ("+ DbTableThrowMode.COL_ID+")";
    public static final String FK_STATUS     = "FOREIGN KEY ("+ COL_STATE_ID +") REFERENCES "+DbTableThrowState.TABLE_NAME+" ("+DbTableThrowState.COL_ID+")";
    public static final String FK_DIRECTION  = "FOREIGN KEY ("+ COL_DIR_ID +") REFERENCES "+ DbTableDirection.TABLE_NAME+" ("+ DbTableDirection.COL_ID+")";


// =================================================================================================
// QUERIES
// =================================================================================================

// CREATE
    public static String createTableQuery() {
        return "CREATE TABLE " + TABLE_NAME + " (" +
                COL_ID + DOMAIN_ID + "," +
                COL_CONTACT_ID + DOMAIN_CONTACT + "," +
                COL_MODE_ID + DOMAIN_MODE + "," +
                COL_TYPE_ID + DOMAIN_FILE_TYPE + "," +
                COL_STATE_ID + DOMAIN_STATE + "," +
                COL_DIR_ID + DOMAIN_DIRECTION + "," +
                COL_TIME_ID + DOMAIN_TIME_ID + "," +
                COL_FILENAME + DOMAIN_FILENAME + "," +
                COL_SIZE + DOMAIN_SIZE + "," +
                COL_MD5 + DOMAIN_MD5 + "," +
                COL_PATH + DOMAIN_PATH + "," +
                FK_CONTACT  + "," +
                FK_FILE_TYPE  + "," +
                FK_MODE + "," +
                FK_STATUS  + "," +
                FK_DIRECTION + ");";
    }

// INSERT
    public static String insertQuery(int contactId, ThrowWrapper throwW) {
        return "INSERT INTO " + TABLE_NAME + " VALUES (null, " +
                contactId + ", " + throwW.getMode() + ", " + throwW.getType() + ", "  + throwW.getState() + ", " +
                throwW.getDir() + ", " + throwW.getTimeID().getTime() + ", '" + throwW.getFilename() + "', " +
                throwW.getFileSize() + ", '" + throwW.getMD5().toString() + "', '" + throwW.getPath() + "');";
    }

// UPDATE
    public static String updateStateQuery(int contactId, Date timeID, Byte state) {
        return "UPDATE " + TABLE_NAME + " SET " + COL_STATE_ID + " = " + state +
                " WHERE " + COL_CONTACT_ID + " == " + contactId + " AND " + COL_TIME_ID + " == " + timeID.getTime() + ";";
    }

    public static String updatePathQuery(int contactId, Date timeID, String path) {
        return "UPDATE " + TABLE_NAME + " SET " + COL_PATH + " = '" + path +
                "' WHERE " + COL_CONTACT_ID + " == " + contactId + " AND " + COL_TIME_ID + " == " + timeID.getTime() + ";";
    }

// DELETE
    private static String deleteQuery(String whereClause) {
        return "DELETE FROM " + TABLE_NAME + " " + whereClause + ";";
    }

    public static String deleteAllQuery() {
        return deleteQuery("");
    }

    public static String deleteByContactIdQuery(int contactId) {
        String whereClause = "WHERE " + COL_CONTACT_ID + " == " + contactId;
        return deleteQuery(whereClause);
    }

    public static String deleteByTimeIdQuery(int contactId, Date timeId) {
        String whereClause = "WHERE " + COL_CONTACT_ID + " == " + contactId + " AND " + COL_TIME_ID + " == " + timeId.getTime();
        return deleteQuery(whereClause);
    }


}