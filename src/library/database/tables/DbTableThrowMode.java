package library.database.tables;


import library.codes.ThrowModeCodes;

import java.util.ArrayList;

public class DbTableThrowMode {

// =================================================================================================
// CONTRACT
// =================================================================================================

    public static final String TABLE_NAME = "modes";

    public static final String COL_ID   = "id";
    public static final String COL_MODE = "mode";

    public static final String DOMAIN_ID    = " INTEGER PRIMARY KEY NOT NULL";
    public static final String DOMAIN_MODE  = " TEXT NOT NULL";


// =================================================================================================
// QUERIES
// =================================================================================================

// CREATE
    public static String createTableQuery() {
        return "CREATE TABLE " + TABLE_NAME + " (" +
                COL_ID + DOMAIN_ID + "," +
                COL_MODE + DOMAIN_MODE + ");";
    }

// INSERT
    private static String insertQuery(byte modeId, String modeTitle) {
        return "INSERT INTO " + TABLE_NAME + " VALUES ("+modeId+", '"+modeTitle +"');";
    }

// FILL
    public static ArrayList<String> fillQueries() {
        ArrayList<String> queries = new ArrayList<>();

        queries.add(insertQuery(ThrowModeCodes.WIFI_NETWORK, ThrowModeCodes.WIFI_NETWORK_TITLE));
        queries.add(insertQuery(ThrowModeCodes.WIFI_DIRECT, ThrowModeCodes.WIFI_DIRECT_TITLE));
        queries.add(insertQuery(ThrowModeCodes.BLUETOOTH, ThrowModeCodes.BLUETOOTH_TITLE));

        return queries;
    }


}
