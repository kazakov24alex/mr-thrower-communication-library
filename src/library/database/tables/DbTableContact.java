package library.database.tables;

import library.exceptions.EncodingException;
import library.exceptions.ParsePacketException;
import library.objects.GreetingObject;
import library.wrappers.IpAddress;
import library.wrappers.MacAddress;
import library.database.RecordReport;

import javax.crypto.Mac;


public class DbTableContact {

// =================================================================================================
// CONTRACT
// =================================================================================================

    public static final String TABLE_NAME = "contacts";

    public static final String COL_ID    = "id";
    public static final String COL_MAC   = "mac";
    public static final String COL_NAME  = "name";
    public static final String COL_DEVICE= "device";
    public static final String COL_OS    = "os";

    public static final String DOMAIN_ID = " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL";
    public static final String DOMAIN_MAC = " BLOB NOT NULL UNIQUE";
    public static final String DOMAIN_NAME = " TEXT NOT NULL";
    public static final String DOMAIN_DEVICE = " TEXT NOT NULL";
    public static final String DOMAIN_OS = " INTEGER NOT NULL";

    public static final String FK_OS = "FOREIGN KEY ("+ COL_OS +") REFERENCES "+DbTableOs.TABLE_NAME+" ("+DbTableOs.COL_ID+")";


// =================================================================================================
// QUERIES
// =================================================================================================

// CREATE
    public static String createTableQuery() {
        return "CREATE TABLE " + TABLE_NAME + " (" +
                COL_ID + DOMAIN_ID + "," +
                COL_MAC + DOMAIN_MAC + "," +
                COL_NAME + DOMAIN_NAME + "," +
                COL_DEVICE + DOMAIN_DEVICE + "," +
                COL_OS + DOMAIN_OS + "," +
                FK_OS + ");";
    }

// INSERT
    public static String insertQuery(GreetingObject greeting) {
        return "INSERT INTO " + TABLE_NAME + " VALUES (null, '" + greeting.getMacAddress().toString() + "', '" +
                greeting.getName() + "', '" + greeting.getDevice() + "', " + greeting.getOs() + ");";
    }

// UPDATE
    public static String updateNameQuery(MacAddress macAddress, String name) {
        return "UPDATE " + TABLE_NAME + " SET " + COL_NAME + " = '" + name + "' WHERE " + COL_MAC + " == '" + macAddress.toString() + "';";
    }

// DELETE
    private static String deleteQuery(String whereClause) {
        return "DELETE FROM " + TABLE_NAME + " " + whereClause + ";";
    }

    public static String deleteAllQuery() {
        return deleteQuery("");
    }

    public static String deleteContactQuery(MacAddress macAddress) {
        String whereClause = "WHERE " + COL_MAC + " == " + "'" + macAddress.toString() + "'";
        return deleteQuery(whereClause);
    }

// SELECT
    private static String selectQuery(String whereClause) {
        return "SELECT * FROM " + TABLE_NAME + " " + whereClause + ";";
    }

    public static String selectAllQuery() {
        String whereClause = "";
        return selectQuery(whereClause);
    }

    public static String selectByMacQuery(MacAddress macAddress) {
        String whereClause = "WHERE " + COL_MAC + " == " + "'" + macAddress.toString() + "'";
        return selectQuery(whereClause);
    }

// =================================================================================================
// DECODING
// =================================================================================================

    public static GreetingObject decodeRecordReport(RecordReport report) throws ParsePacketException, EncodingException {
        return new GreetingObject(
                new MacAddress(report.getField(1)),
                new IpAddress(),
                report.getField(2),
                report.getField(3),
                Byte.parseByte(report.getField(4))
        );
    }

    public static int decodeContactId(RecordReport report) {
        return Integer.parseInt(report.getField(0));
    }


}
