package library.database;

import library.database.tables.*;
import library.database.views.DbViewMessage;
import library.database.views.DbViewThrow;

import java.util.ArrayList;


public class Database {

// =================================================================================================
// CONTRACT
// =================================================================================================

    public static final String DATABASE_NAME = "story.db";


// =================================================================================================
// QUERIES
// =================================================================================================

    public static ArrayList<String> tableInitQuery() {
        ArrayList<String> queries = new ArrayList<>();

        queries.add(DbTableOs.createTableQuery());
        queries.add(DbTableThrowType.createTableQuery());
        queries.add(DbTableThrowMode.createTableQuery());
        queries.add(DbTableThrowState.createTableQuery());
        queries.add(DbTableDirection.createTableQuery());
        queries.add(DbTableContact.createTableQuery());
        queries.add(DbTableMessage.createTableQuery());
        queries.add(DbTableThrow.createTableQuery());

        return queries;
    }

    public static ArrayList<String> viewInitQuery () {
        ArrayList<String> queries = new ArrayList<>();

        queries.add(DbViewMessage.createTableQuery());
        queries.add(DbViewThrow.createTableQuery());

        return queries;
    }

    public static ArrayList<String> fillTablesQuery() {
        ArrayList<String> queries = new ArrayList<>();

        queries.addAll(DbTableOs.fillQueries());
        queries.addAll(DbTableThrowMode.fillQueries());
        queries.addAll(DbTableThrowType.fillQueries());
        queries.addAll(DbTableThrowState.fillQueries());
        queries.addAll(DbTableDirection.fillQueries());

        return queries;
    }


}
