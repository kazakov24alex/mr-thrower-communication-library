package library;

import library.exceptions.EncodingException;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


/**
 * Static class-utility for converting primitive and object arrays.
 * @author Alex Kazakov
 * @version TESTED (coverage 100%)
 */
public class ByteUtils {


//======================================================================================================================
// TO BYTE-OBJECTS
//======================================================================================================================

    /**
     * Convert primitive-byte array to object-byte array.
     * @param array - primitive-byte array
     * @return object-byte array
     */
    public static Byte[] bytesToObjects(byte[] array) {
        Byte[] bytesObjArr = new Byte[array.length];

        for(int i = 0; i < array.length; i++) {
            bytesObjArr[i] = array[i];
        }

        return bytesObjArr;
    }

    /**
     * Convert short number to object-byte array with size = 2.
     * @param num - short number
     * @return object-byte array with size = 2
     */
    public static Byte[] shortToBytes(short num) {
        return bytesToObjects(ByteBuffer.allocate(2).putShort(num).array());
    }

    /**
     * Convert int number to object-byte array with size = 4.
     * @param num - int number
     * @return object-byte array with size = 4
     */
    public static Byte[] intToBytes(int num) {
        return bytesToObjects(ByteBuffer.allocate(4).putInt(num).array());
    }

    /**
     * Convert long number to object-byte array with size = 8.
     * @param num - long number
     * @return object-byte array with size = 8
     */
    public static Byte[] longToBytes(long num) {
        return bytesToObjects(ByteBuffer.allocate(8).putLong(num).array());
    }

    /**
     * Convert string to object-byte array encoded UTF-8.
     * @param str - string
     * @exception EncodingException incorrect string
     * @return object-byte array
     */
    public static Byte[] stringToBytes(String str) {
        if(str == null)
            return null;

        try {
            return bytesToObjects(str.getBytes("UTF-8"));
        } catch (Exception e) {
            System.err.println("Encoding String to Byte[] - incorrect String");
            return null;
        }
    }


//======================================================================================================================
// FROM OBJECTS
//======================================================================================================================

    /**
     * Convert object-byte array to primitive-byte array.
     * @param oBytes - object-byte array
     * @return primitive-byte array
     */
    public static byte[] toPrimitives(Byte[] oBytes) {
        byte[] bytes = new byte[oBytes.length];

        for(int i = 0; i < oBytes.length; i++) {
            bytes[i] = oBytes[i];
        }

        return bytes;
    }

    /**
     * Convert object-byte array with size = 2 to short number.
     * @param bytes - object-byte array
     * @exception EncodingException incorrect size of array
     * @return short number
     */
    public static short toShort(Byte[] bytes) throws EncodingException {
        if(bytes.length != 2)
            throw new EncodingException("Encoding Byte[] to short - Byte[] contains not 2 bytes");

        ByteBuffer bb = ByteBuffer.allocate(2);
        bb.order(ByteOrder.BIG_ENDIAN);
        for(Byte b : bytes) {
            bb.put(b);
        }

        return bb.getShort(0);
    }

    /**
     * Convert object-byte array with size = 4 to int number.
     * @param bytes - object-byte array
     * @exception EncodingException incorrect size of array
     * @return int number
     */
    public static int toInt(Byte[] bytes) throws EncodingException {
        if(bytes.length != 4)
            throw new EncodingException("Encoding Byte[] to int - Byte[] contains not 4 bytes");

        ByteBuffer bb = ByteBuffer.allocate(4);
        bb.order(ByteOrder.BIG_ENDIAN);
        for(Byte b : bytes) {
            bb.put(b);
        }

        return bb.getInt(0);
    }

    /**
     * Convert object-byte array with size = 8 to long number.
     * @param bytes - object-byte array
     * @exception EncodingException incorrect size of array
     * @return long number
     */
    public static long toLong(Byte[] bytes) throws EncodingException {
        if(bytes.length != 8)
            throw new EncodingException("Encoding Byte[] to Long - Byte[] contains not 8 bytes");

        ByteBuffer bb = ByteBuffer.allocate(8);
        bb.order(ByteOrder.BIG_ENDIAN);
        for(Byte b : bytes) {
            bb.put(b);
        }

        return bb.getLong(0);
    }

    /**
     * Convert object-byte array encoded UTF-8 to string.
     * @param bytes - object-byte array
     * @exception EncodingException incorrect size of array
     * @return string
     */
    public static String toString(Byte[] bytes) throws EncodingException {
        try {
            return new String(toPrimitives(bytes), "UTF-8");
        } catch (Exception ex) {
            throw new EncodingException("Encoding Byte[] to String - incorrect Byte[]");
        }
    }


//======================================================================================================================
// BYTE NUMBER GETTER
//======================================================================================================================

    /**
     * Calculate number of bytes for string encoded UTF-8.
     * @param str - string
     * @exception EncodingException incorrect string
     * @return number of bytes
     */
    public static int getBytesNum(String str)  {
        if(str == null)
            return 0;

        try {
            return bytesToObjects(str.getBytes("UTF-8")).length;
        } catch (UnsupportedEncodingException e) {
            System.err.println("Encoding String to Byte[] - incorrect String");
            return 0;
        }
    }

}
