package library.interfaces.event_manager;

import library.BytePacket;
import library.objects.ThrowEndAckObject;
import library.objects.ThrowProgressObject;
import library.objects.ThrowResponseObject;
import library.wrappers.IpAddress;
import library.wrappers.MacAddress;


public interface IEventThrow {

    IpAddress getIpAddressByMac(MacAddress sender);

    void sendThrowResponse(ThrowResponseObject response);
    void sendThrowProgress(ThrowProgressObject progress);
    void sendThrowEndAck(ThrowEndAckObject endAck, String path);

}
