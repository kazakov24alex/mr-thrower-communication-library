package library.interfaces.event_manager;

import library.objects.*;
import library.wrappers.EventWrapper;


public interface IEventListener {

    void onEvent(EventWrapper event);

    void onGreetingEvent(GreetingObject greeting);
    void onGoodbyeEvent(GoodbyeObject goodbye);

    void onMessageEvent(MessageObject message);
    void onMessageAckEvent(MessageAckObject messageAck);

    void onThrowRequestEvent(ThrowRequestObject request);
    void onThrowResponseEvent(ThrowResponseObject response);
    void onThrowProgressEvent(ThrowProgressObject progress);
    void onThrowEndAckEvent(ThrowEndAckObject throwEnd);

}
