package library.interfaces.event_manager;


import library.BytePacket;
import library.wrappers.MacAddress;

public interface IEventReceiver {

    void onReceivedPacket(MacAddress sender, BytePacket bytePacket);

    void onReceivedGreeting(BytePacket packet);
    void onReceivedGreetingAck(BytePacket packet);
    void onReceivedGoodbye(BytePacket packet);

    void onReceivedMessage(MacAddress sender, BytePacket packet);
    void onReceivedMessageAck(MacAddress sender, BytePacket packet);

    void onReceivedThrowRequest(MacAddress sender, BytePacket packet);
    void onReceivedThrowResponse(MacAddress sender, BytePacket packet);
    void onReceivedThrowProgress(MacAddress sender, BytePacket packet);
    void onReceivedThrowEndAck(MacAddress sender, BytePacket packet);

}
