package library.interfaces.event_manager;

import library.objects.*;
import library.wrappers.EventWrapper;

public interface IEventCreator {

    void sendEvent(EventWrapper event);

    void sendGreetingAck(GreetingObject greeting);
    void sendMessage(MessageObject message);
    void sendMessageAck(MessageAckObject messageAck);
    void sendThrowRequest(ThrowRequestObject request);
    void sendThrowResponse(ThrowResponseObject response);

    void catchThrowRequest(ThrowRequestObject request);
    void getActualContacts();

}
