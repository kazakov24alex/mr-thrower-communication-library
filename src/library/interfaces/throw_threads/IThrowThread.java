package library.interfaces.throw_threads;

import java.util.Date;

public interface IThrowThread {

    Date getTimeID();

    void activate();
    void deactivate();

}
