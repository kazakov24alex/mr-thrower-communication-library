package library.interfaces.throw_threads;

public interface ICatchThread {

    void activate();
    void deactivate();

}
