package library.interfaces.throw_manager;

import library.objects.ThrowEndAckObject;
import library.objects.ThrowProgressObject;
import library.objects.ThrowResponseObject;
import library.wrappers.IpAddress;
import library.wrappers.MacAddress;

import java.util.Date;


public interface IThrowManagerInformer {

    IpAddress getIpAddressByMac(MacAddress macAddress);

    void finishCatch(MacAddress macAddress, Date timeID);

    void transmitThrowEndAck(ThrowEndAckObject endAck, String path);

    void noContact(MacAddress addressee);

}
