package library.interfaces.throw_manager;


import library.objects.ThrowFileObject;
import library.wrappers.MacAddress;

import java.util.Date;
import java.util.List;

public interface IThrowManager {

    void stop();

    void startThrowIfNotYet(MacAddress macAddress, int port);
    void startCatchIfNotYet(MacAddress macAddress);
    void finishThrow(MacAddress macAddress, Date timeID);

    void addThrowFiles(List<ThrowFileObject> tFileList);
    void addCatchFiles(List<ThrowFileObject> tFileList);

    void kickUserFromThrows(MacAddress macAddress);
    void kickUserFromCatches(MacAddress macAddress);

}
