package library.interfaces;

import library.BytePacket;
import library.wrappers.MacAddress;

import java.util.ArrayList;


public interface IConnectionManager {

    void activate();
    void deactivate();

    void specifyMacToConnection(MacAddress macAddress);
    void sendPacket(MacAddress macAddress, BytePacket packet);

    ArrayList<MacAddress> getActualMacAddresses();

}
