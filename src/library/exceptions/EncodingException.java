package library.exceptions;


/**
 * Exception-class for throwing when errors occur while converting data.
 * @author Alex Kazakov
 */
public class EncodingException extends Exception {

    public EncodingException(String message) {
        super(message);
    }

}
