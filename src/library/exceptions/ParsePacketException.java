package library.exceptions;


/**
 * Exception-class for throwing when errors occur while parsing a byte package.
 * @author Alex Kazakov
 */
public class ParsePacketException extends Exception {

    public ParsePacketException(String message) {
        super(message);
    }

}
