package library.exceptions;


/**
 * Exception-class for throwing when errors occur while building a byte package.
 * @author Alex Kazakov
 */
public class BuildPacketException extends Exception {

    public BuildPacketException(String message) {
        super(message);
    }

}
