# Mr Thrower library

## About the library
Author         | Alex Kazakov / <kazakov24alex@gmail.com>
-------------- | --------------
Language	   | Java
DB language    | SQL
Test framework | JUnit 5
Android client | <https://bitbucket.org/kazakov24alex/mr-thrower-android-client/src>
Windows client | <https://bitbucket.org/kazakov24alex/mr-thrower-windows-client/src>

## Library ontains:
+ SQL commands of the database
+ wrappers
+ communication protocol
 	+ protocol codes
	+ protocol objects
	+ protocol packet
+ interfaces of data transfer mechanism
+ unit tests
