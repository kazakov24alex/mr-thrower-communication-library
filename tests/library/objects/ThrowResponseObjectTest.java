package library.objects;

import library.BytePacket;
import library.ByteUtils;
import library.codes.PacketCodes;
import library.exceptions.EncodingException;
import library.exceptions.ParsePacketException;
import library.wrappers.MacAddress;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

class ThrowResponseObjectTest {

    private MacAddress testMacAddress;

    private Date testTimeID;
    private int testPort;


    @BeforeEach
    void setUp() throws EncodingException {
        byte[] macBytes = { 0, 1, 2, 3, 4, 5 };
        testMacAddress = new MacAddress(macBytes);

        testTimeID = new Date();
        testPort = 1234;
    }

    @AfterEach
    void tearDown() {
    }


    @Test
    void testPOJO() {
        ThrowResponseObject response = new ThrowResponseObject(testTimeID, testPort);
        response.setAddress(testMacAddress);

        Assertions.assertEquals(testTimeID, response.getTimeID());
        Assertions.assertEquals(testPort, response.getPort());
        Assertions.assertEquals(0, testMacAddress.compareTo(response.getAddress()));
    }

    @Test
    void testToString() {
        ThrowResponseObject responseFirst = new ThrowResponseObject(testTimeID, testPort);
        ThrowResponseObject responseSecond = new ThrowResponseObject(testTimeID, testPort);

        Assertions.assertEquals(responseFirst.toString(), responseSecond.toString());

        ThrowResponseObject responseThird = new ThrowResponseObject(testTimeID, testPort);
        responseThird.setAddress(testMacAddress);
        ThrowResponseObject responseFourth = new ThrowResponseObject(testTimeID, testPort);
        responseFourth.setAddress(testMacAddress);

        Assertions.assertEquals(responseThird.toString(), responseFourth.toString());
    }


    @Test
    void testThrowResponse() throws ParsePacketException, EncodingException {
        ThrowResponseObject testResponse = new ThrowResponseObject(testTimeID, testPort);
        testResponse.setAddress(testMacAddress);
        BytePacket packet = testResponse.getBytePacket();
        ThrowResponseObject returnThrowResponse = new ThrowResponseObject(packet, testMacAddress);

        byte code = packet.getByte(0);
        short recordedLength = ByteUtils.toShort(packet.getBytes(1, 2));
        Assertions.assertEquals(PacketCodes.THROW_RESPONSE, code);
        Assertions.assertEquals(packet.getData().length, recordedLength);

        Assertions.assertEquals(testResponse.getTimeID(), returnThrowResponse.getTimeID());
        Assertions.assertEquals(testResponse.getPort(), returnThrowResponse.getPort());
        Assertions.assertEquals(testResponse.toString(), returnThrowResponse.toString());
        Assertions.assertEquals(testResponse.isNegative(), returnThrowResponse.isNegative());
    }

    @Test
    void incorrectThrowResponseParse() throws EncodingException {
        ThrowResponseObject testResponse = new ThrowResponseObject(testTimeID, testPort);
        BytePacket packet = testResponse.getBytePacket();
        packet.takeByte();

        Assertions.assertThrows(ParsePacketException.class, () -> {
            new ThrowResponseObject(packet, testMacAddress);
        });
    }


}