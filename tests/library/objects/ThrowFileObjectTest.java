package library.objects;

import library.exceptions.EncodingException;
import library.wrappers.MD5;
import library.wrappers.MacAddress;
import org.junit.jupiter.api.*;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

class ThrowFileObjectTest {

    private MacAddress testMacAddress;

    private Date testTimeID;
    private String testFilename;
    private long testFileSize;
    private MD5 testMD5;

    private String testPath;


    @BeforeEach
    void setUp() throws EncodingException {
        byte[] macBytes = { 0, 1, 2, 3, 4, 5 };
        byte[] md5Bytes = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };

        testMacAddress = new MacAddress(macBytes);

        testTimeID = new Date();
        testFilename = "TEST_file_имя";
        testPath = "TEST_path";

        testFileSize = 77777777;
        testMD5 = new MD5(md5Bytes);
    }

    @AfterEach
    void tearDown() {
    }


    @Test
    void testPOJO() {
        ThrowFileObject throwFile = new ThrowFileObject(testTimeID, testFilename, testFileSize, testMD5);
        throwFile.setPath(testPath);
        throwFile.setAddress(testMacAddress);

        Assertions.assertEquals(testTimeID, throwFile.getTimeID());
        Assertions.assertEquals(testFilename, throwFile.getFilename());
        Assertions.assertEquals(testFileSize, throwFile.getFileSize());
        Assertions.assertEquals(0, testMD5.compareTo(throwFile.getMD5()));
        Assertions.assertEquals(testPath, throwFile.getPath());
        Assertions.assertEquals(0, testMacAddress.compareTo(throwFile.getAddress()));
    }

    @Test
    void testToString() {
        ThrowFileObject throwFileFirst = new ThrowFileObject(testTimeID, testFilename, testFileSize, testMD5);
        ThrowFileObject throwFileSecond = new ThrowFileObject(testTimeID, testFilename, testFileSize, testMD5);

        Assertions.assertEquals(throwFileFirst.toString(), throwFileSecond.toString());

        ThrowFileObject throwFileThird = new ThrowFileObject(testTimeID, testFilename, testFileSize, testMD5);
        throwFileThird.setAddress(testMacAddress);
        ThrowFileObject throwFileFourth = new ThrowFileObject(testTimeID, testFilename, testFileSize, testMD5);
        throwFileFourth.setAddress(testMacAddress);

        Assertions.assertEquals(throwFileThird.toString(), throwFileFourth.toString());
    }


    @Test
    void testUUIDstart() {
        Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
        calendar.set(2018, 5, 25, 21, 7, 5);

        ThrowFileObject throwFile = new ThrowFileObject(calendar.getTime(), testFilename, testFileSize, testMD5);

        System.out.println(throwFile.getUUIDstart());
    }

}