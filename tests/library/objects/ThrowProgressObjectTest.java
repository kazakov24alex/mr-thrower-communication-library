package library.objects;

import library.BytePacket;
import library.ByteUtils;
import library.codes.PacketCodes;
import library.exceptions.EncodingException;
import library.exceptions.ParsePacketException;
import library.wrappers.MacAddress;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

class ThrowProgressObjectTest {

    private MacAddress testMacAddress;

    private Date testTimeID;
    private long testProgress;


    @BeforeEach
    void setUp() throws EncodingException {
        byte[] macBytes = { 0, 1, 2, 3, 4, 5 };
        testMacAddress = new MacAddress(macBytes);

        testTimeID = new Date();
        testProgress = 1234567890;
    }

    @AfterEach
    void tearDown() {
    }


    @Test
    void testPOJO() {
        ThrowProgressObject throwProgress = new ThrowProgressObject(testTimeID, testProgress);
        throwProgress.setAddress(testMacAddress);

        Assertions.assertEquals(testTimeID, throwProgress.getTimeID());
        Assertions.assertEquals(testProgress, throwProgress.getValue());
        Assertions.assertEquals(0, testMacAddress.compareTo(throwProgress.getAddress()));
    }


    @Test
    void testToString() {
        ThrowProgressObject throwProgressFirst = new ThrowProgressObject(testTimeID, testProgress);
        ThrowProgressObject throwProgressSecond = new ThrowProgressObject(testTimeID, testProgress);

        Assertions.assertEquals(throwProgressFirst.toString(), throwProgressSecond.toString());

        ThrowProgressObject throwProgressThird = new ThrowProgressObject(testTimeID, testProgress);
        throwProgressThird.setAddress(testMacAddress);
        ThrowProgressObject throwProgressFourth = new ThrowProgressObject(testTimeID, testProgress);
        throwProgressFourth.setAddress(testMacAddress);

        Assertions.assertEquals(throwProgressThird.toString(), throwProgressFourth.toString());
    }


    @Test
    void testThrowProgress() throws ParsePacketException, EncodingException {
        ThrowProgressObject testThrowProgress = new ThrowProgressObject(testTimeID, testProgress);
        testThrowProgress.setAddress(testMacAddress);
        BytePacket packet = testThrowProgress.getBytePacket();
        ThrowProgressObject returnThrowProgress = new ThrowProgressObject(packet, testMacAddress);

        byte code = packet.getByte(0);
        short recordedLength = ByteUtils.toShort(packet.getBytes(1, 2));
        Assertions.assertEquals(PacketCodes.THROW_PROGRESS, code);
        Assertions.assertEquals(packet.getData().length, recordedLength);

        Assertions.assertEquals(testThrowProgress.getTimeID(), returnThrowProgress.getTimeID());
        Assertions.assertEquals(testThrowProgress.getValue(), returnThrowProgress.getValue());
        Assertions.assertEquals(testThrowProgress.toString(), returnThrowProgress.toString());
    }

    @Test
    void incorrectThrowProgressParse() throws EncodingException {
        ThrowProgressObject testThrowProgress = new ThrowProgressObject(testTimeID, testProgress);
        BytePacket packet = testThrowProgress.getBytePacket();
        packet.takeByte();

        Assertions.assertThrows(ParsePacketException.class, () -> {
            new ThrowProgressObject(packet, testMacAddress);
        });
    }

}