package library.objects;

import library.BytePacket;
import library.ByteUtils;
import library.codes.PacketCodes;
import library.exceptions.EncodingException;
import library.exceptions.ParsePacketException;
import library.wrappers.MacAddress;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

class MessageObjectTest {

    private MacAddress testMacAddress;

    private Date testTimeID;
    private String testText;


    @BeforeEach
    void setUp() throws EncodingException {
        byte[] macBytes = { 0, 1, 2, 3, 4, 5 };
        testMacAddress = new MacAddress(macBytes);

        testTimeID = new Date();
        testText = "TEST_message_текст";
    }

    @AfterEach
    void tearDown() {
    }


    @Test
    void testPOJO() {
        MessageObject message = new MessageObject(testTimeID, testText);
        message.setAddress(testMacAddress);

        Assertions.assertEquals(testTimeID, message.getTimeID());
        Assertions.assertEquals(testText, message.getText());
        Assertions.assertEquals(0, testMacAddress.compareTo(message.getAddress()));
    }


    @Test
    void testToString() {
        MessageObject messageOne = new MessageObject(testTimeID, testText);
        MessageObject messageTwo = new MessageObject(testTimeID, testText);

        Assertions.assertEquals(messageOne.toString(), messageTwo.toString());

        MessageObject messageThird = new MessageObject(testTimeID, testText);
        messageThird.setAddress(testMacAddress);
        MessageObject messageFourth = new MessageObject(testTimeID, testText);
        messageFourth.setAddress(testMacAddress);

        Assertions.assertEquals(messageThird.toString(), messageFourth.toString());
    }


    @Test
    void testProtocol() throws ParsePacketException, EncodingException {
        MessageObject testMessage = new MessageObject(testTimeID, testText);
        testMessage.setAddress(testMacAddress);
        BytePacket packet = testMessage.getBytePacket();
        MessageObject returnTextMessage = new MessageObject(packet, testMacAddress);;

        byte code = packet.getByte(0);
        short recordedLength = ByteUtils.toShort(packet.getBytes(1, 2));
        Assertions.assertEquals(PacketCodes.MESSAGE, code);
        Assertions.assertEquals(packet.getData().length, recordedLength);

        Assertions.assertEquals(testMessage.getText(), returnTextMessage.getText());
        Assertions.assertEquals(testMessage.getTimeID(), returnTextMessage.getTimeID());
        Assertions.assertEquals(testMessage.toString(), returnTextMessage.toString());
    }


    @Test
    void incorrectTestProtocol() throws EncodingException {
        MessageObject testMessage = new MessageObject(testTimeID, testText);
        BytePacket packet = testMessage.getBytePacket();
        packet.takeByte();

        Assertions.assertThrows(ParsePacketException.class, () -> {
           new MessageObject(packet, testMacAddress);
        });
    }


}