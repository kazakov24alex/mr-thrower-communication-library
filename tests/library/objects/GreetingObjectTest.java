package library.objects;

import library.BytePacket;
import library.ByteUtils;
import library.codes.PacketCodes;
import library.exceptions.BuildPacketException;
import library.exceptions.EncodingException;
import library.exceptions.ParsePacketException;
import library.wrappers.IpAddress;
import library.wrappers.MacAddress;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GreetingObjectTest {

    private MacAddress testMacAddress;
    private IpAddress testIpAddress;
    private String testName;
    private String testDevice;
    private Byte testOSwindows;
    private Byte testOSandroid;


    @BeforeEach
    void setUp() throws EncodingException {
        byte[] macBytes = { 0, 1, 2, 3, 4, 5 };
        byte[] ipBytes = { 0, 1, 2, 3 };

        testMacAddress = new MacAddress(macBytes);
        testIpAddress = new IpAddress(ipBytes);
        testName = "TEST_name";
        testDevice = "TEST_device";
        testOSwindows = GreetingObject.OS_CODE_WINDOWS;
        testOSandroid = GreetingObject.OS_CODE_ANDROID;

    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testPOJO() {
        GreetingObject greeting = new GreetingObject(testMacAddress, testIpAddress, testName, testDevice, testOSwindows);

        Assertions.assertEquals(greeting.getCode(), PacketCodes.GREETING);
        Assertions.assertEquals(0, testIpAddress.compareTo(greeting.getIpAddress()));
        Assertions.assertEquals(0, testMacAddress.compareTo(greeting.getMacAddress()));
        Assertions.assertEquals(testName, greeting.getName());
        Assertions.assertEquals(testDevice, greeting.getDevice());
        Assertions.assertEquals(testOSwindows, greeting.getOs());
    }

    @Test
    void testToString() {
        GreetingObject greetingOne = new GreetingObject(testMacAddress, testIpAddress, testName, testDevice, testOSandroid);
        GreetingObject greetingTwo = new GreetingObject(testMacAddress, testIpAddress, testName, testDevice, testOSandroid);

        Assertions.assertEquals(greetingOne.toString(), greetingTwo.toString());

        GreetingObject greetingThird = new GreetingObject(testMacAddress, testIpAddress, testName, testDevice, testOSwindows);
        GreetingObject greetingFourth = new GreetingObject(testMacAddress, testIpAddress, testName, testDevice, testOSwindows);

        Assertions.assertEquals(greetingThird.toString(), greetingFourth.toString());
    }


    @Test
    void testProtocol() throws BuildPacketException, ParsePacketException, EncodingException {
        GreetingObject testGreeting = new GreetingObject(testMacAddress, testIpAddress, testName, testDevice, testOSandroid);

        BytePacket packet = testGreeting.getBytePacket();
        GreetingObject returnGreeting = new GreetingObject(packet);

        byte code = packet.getByte(0);
        short recordedLength = ByteUtils.toShort(packet.getBytes(1, 2));
        Assertions.assertEquals(PacketCodes.GREETING, code);
        Assertions.assertEquals(packet.getData().length, recordedLength);

        Assertions.assertEquals(0, testGreeting.getMacAddress().compareTo(returnGreeting.getMacAddress()));
        Assertions.assertEquals(0, testGreeting.getIpAddress().compareTo(returnGreeting.getIpAddress()));
        Assertions.assertEquals(testGreeting.getName(), returnGreeting.getName());
        Assertions.assertEquals(testGreeting.getDevice(), returnGreeting.getDevice());
        Assertions.assertEquals(testGreeting.getOs(), returnGreeting.getOs());
        Assertions.assertEquals(testGreeting.toString(), returnGreeting.toString());
    }

    @Test
    void incorrectTestProtocol() throws BuildPacketException, EncodingException {
        GreetingObject testGreeting = new GreetingObject(testMacAddress, testIpAddress, testName, testDevice, testOSandroid);
        BytePacket packet = testGreeting.getBytePacket();
        packet.takeByte();

        Assertions.assertThrows(ParsePacketException.class, () -> {
            new GreetingObject(packet);
        });
    }

    @Test
    void testProtocolAck() throws BuildPacketException, ParsePacketException, EncodingException {
        GreetingObject testGreetingAck = new GreetingObject(testMacAddress, testIpAddress, testName, testDevice, testOSandroid);

        BytePacket packet = testGreetingAck.getAckBytePacket();
        GreetingObject returnGreetingAck = new GreetingObject(packet);

        byte code = packet.getByte(0);
        short recordedLength = ByteUtils.toShort(packet.getBytes(1, 2));
        Assertions.assertEquals(PacketCodes.GREETING_ACK, code);
        Assertions.assertEquals(packet.getData().length, recordedLength);

        Assertions.assertEquals(0, testGreetingAck.getMacAddress().compareTo(returnGreetingAck.getMacAddress()));
        Assertions.assertEquals(0, testGreetingAck.getIpAddress().compareTo(returnGreetingAck.getIpAddress()));
        Assertions.assertEquals(testGreetingAck.getName(), returnGreetingAck.getName());
        Assertions.assertEquals(testGreetingAck.getDevice(), returnGreetingAck.getDevice());
        Assertions.assertEquals(testGreetingAck.getOs(), returnGreetingAck.getOs());
        Assertions.assertEquals(testGreetingAck.toString(), returnGreetingAck.toString());
    }


    @Test
    void incorrectTestProtocolAck() throws BuildPacketException, EncodingException {
        GreetingObject testGreetingAck = new GreetingObject(testMacAddress, testIpAddress, testName, testDevice, testOSandroid);
        BytePacket packet = testGreetingAck.getAckBytePacket();
        packet.takeByte();

        Assertions.assertThrows(ParsePacketException.class, () -> {
            new GreetingObject(packet);
        });
    }

}