package library.objects;

import library.BytePacket;
import library.ByteUtils;
import library.codes.PacketCodes;
import library.exceptions.BuildPacketException;
import library.exceptions.EncodingException;
import library.exceptions.ParsePacketException;
import library.wrappers.MacAddress;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

class MessageAckObjectTest {

    private Date testTimeID;
    private MacAddress testMacAddress;

    private byte testErrorCodeSuccess;
    private String testErrorMessageSuccess;

    private byte testErrorCodeFailure;
    private String testErrorMessageFailure;


    @BeforeEach
    void setUp() throws EncodingException {
        byte[] macBytes = { 0, 1, 2, 3, 4, 5 };

        testTimeID = new Date();
        testMacAddress = new MacAddress(macBytes);

        testErrorCodeSuccess = MessageAckObject.CODE_SUCCESS;
        testErrorCodeFailure = MessageAckObject.CODE_FAILURE;
        testErrorMessageSuccess = null;
        testErrorMessageFailure = "TEST_ERROR_MESSAGE";
    }

    @AfterEach
    void tearDown() {
    }


    @Test
    void testPOJO() {
        MessageAckObject messageAck = new MessageAckObject(testTimeID, testErrorCodeSuccess, testErrorMessageSuccess);
        messageAck.setAddress(testMacAddress);

        Assertions.assertEquals(testTimeID, messageAck.getTimeID());
        Assertions.assertEquals(testErrorCodeSuccess, messageAck.getErrorCode());
        Assertions.assertEquals(testErrorMessageSuccess, messageAck.getErrorMessage());
        Assertions.assertEquals(0, testMacAddress.compareTo(messageAck.getAddress()));
    }

    @Test
    void testToString() {
        MessageAckObject messageAckFirst = new MessageAckObject(testTimeID, testErrorCodeSuccess, testErrorMessageSuccess);
        MessageAckObject messageAckSecond = new MessageAckObject(testTimeID, testErrorCodeSuccess, testErrorMessageSuccess);

        Assertions.assertEquals(messageAckFirst.toString(), messageAckSecond.toString());

        MessageAckObject messageAckThird = new MessageAckObject(testTimeID, testErrorCodeFailure, testErrorMessageFailure);
        messageAckThird.setAddress(testMacAddress);
        MessageAckObject messageAckFourth = new MessageAckObject(testTimeID, testErrorCodeFailure, testErrorMessageFailure);
        messageAckFourth.setAddress(testMacAddress);

        Assertions.assertEquals(messageAckThird.toString(), messageAckFourth.toString());
    }


    @Test
    void testProtocolSuccess() throws ParsePacketException, EncodingException {
        MessageAckObject testMessageAck = new MessageAckObject(testTimeID, testErrorCodeSuccess, testErrorMessageSuccess);
        testMessageAck.setAddress(testMacAddress);
        BytePacket packet = testMessageAck.getBytePacket();
        MessageAckObject returnMessageAck = new MessageAckObject(packet, testMacAddress);

        byte code = packet.getByte(0);
        short recordedLength = ByteUtils.toShort(packet.getBytes(1, 2));
        Assertions.assertEquals(PacketCodes.MESSAGE_ACK, code);
        Assertions.assertEquals(packet.getData().length, recordedLength);

        Assertions.assertEquals(testMessageAck.getTimeID(), returnMessageAck.getTimeID());
        Assertions.assertEquals(testMessageAck.isNoErrors(), returnMessageAck.isNoErrors());
        Assertions.assertEquals(testMessageAck.getErrorCode(), returnMessageAck.getErrorCode());
        Assertions.assertEquals(testMessageAck.getErrorMessage(), returnMessageAck.getErrorMessage());
        Assertions.assertEquals(testMessageAck.toString(), returnMessageAck.toString());
    }

    @Test
    void testProtocolFailure() throws ParsePacketException, EncodingException {
        MessageAckObject testMessageAck = new MessageAckObject(testTimeID, testErrorCodeFailure, testErrorMessageFailure);
        testMessageAck.setAddress(testMacAddress);
        BytePacket packet = testMessageAck.getBytePacket();
        MessageAckObject returnMessageAck = new MessageAckObject(packet, testMacAddress);

        byte code = packet.getByte(0);
        short recordedLength = ByteUtils.toShort(packet.getBytes(1, 2));
        Assertions.assertEquals(PacketCodes.MESSAGE_ACK, code);
        Assertions.assertEquals(packet.getData().length, recordedLength);

        Assertions.assertEquals(testMessageAck.getTimeID(), returnMessageAck.getTimeID());
        Assertions.assertEquals(testMessageAck.isNoErrors(), returnMessageAck.isNoErrors());
        Assertions.assertEquals(testMessageAck.getErrorCode(), returnMessageAck.getErrorCode());
        Assertions.assertEquals(testMessageAck.getErrorMessage(), returnMessageAck.getErrorMessage());
        Assertions.assertEquals(testMessageAck.toString(), returnMessageAck.toString());
    }

    @Test
    void incorrectTestProtocol() throws BuildPacketException, EncodingException {
        MessageAckObject testMessageAck = new MessageAckObject(testTimeID, testErrorCodeSuccess, testErrorMessageSuccess);
        BytePacket packet = testMessageAck.getBytePacket();
        packet.takeByte();

        Assertions.assertThrows(ParsePacketException.class, () -> {
            new MessageAckObject(packet, testMacAddress);
        });
    }

}