package library.objects;

import library.BytePacket;
import library.ByteUtils;
import library.codes.PacketCodes;
import library.exceptions.EncodingException;
import library.exceptions.ParsePacketException;
import library.wrappers.MacAddress;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

class ThrowEndAckObjectTest {

    private MacAddress testMacAddress;

    private Date testTimeID;
    private boolean testSuccess;
    private boolean testUnsuccess;


    @BeforeEach
    void setUp() throws EncodingException {
        byte[] macBytes = { 0, 1, 2, 3, 4, 5 };
        testMacAddress = new MacAddress(macBytes);

        testTimeID = new Date();
        testSuccess = true;
        testUnsuccess = false;
    }

    @AfterEach
    void tearDown() {
    }


    @Test
    void testPOJO() {
        ThrowEndAckObject throwEndAck = new ThrowEndAckObject(testTimeID, testSuccess);
        throwEndAck.setAddress(testMacAddress);

        Assertions.assertEquals(testTimeID, throwEndAck.getTimeID());
        Assertions.assertEquals(testSuccess, throwEndAck.isSuccess());
        Assertions.assertEquals(0, testMacAddress.compareTo(throwEndAck.getAddress()));
    }


    @Test
    void testToString() {
        ThrowEndAckObject throwEndAckFirst = new ThrowEndAckObject(testTimeID, testUnsuccess);
        ThrowEndAckObject throwEndAckSecond = new ThrowEndAckObject(testTimeID, testUnsuccess);

        Assertions.assertEquals(throwEndAckFirst.toString(), throwEndAckSecond.toString());

        ThrowEndAckObject throwEndAckThird = new ThrowEndAckObject(testTimeID, testSuccess);
        throwEndAckThird.setAddress(testMacAddress);
        ThrowEndAckObject throwEndAckFourth = new ThrowEndAckObject(testTimeID, testSuccess);
        throwEndAckFourth.setAddress(testMacAddress);

        Assertions.assertEquals(throwEndAckThird.toString(), throwEndAckFourth.toString());
    }


    @Test
    void testProtocol() throws ParsePacketException, EncodingException {
        ThrowEndAckObject testThrowEndAck = new ThrowEndAckObject(testTimeID, testSuccess);
        testThrowEndAck.setAddress(testMacAddress);
        BytePacket packet = testThrowEndAck.getBytePacket();
        ThrowEndAckObject returnThrowEndAck = new ThrowEndAckObject(packet, testMacAddress);

        byte code = packet.getByte(0);
        short recordedLength = ByteUtils.toShort(packet.getBytes(1, 2));
        Assertions.assertEquals(PacketCodes.THROW_END_ACK, code);
        Assertions.assertEquals(packet.getData().length, recordedLength);

        Assertions.assertEquals(testThrowEndAck.getTimeID(), returnThrowEndAck.getTimeID());
        Assertions.assertEquals(testThrowEndAck.isSuccess(), returnThrowEndAck.isSuccess());
        Assertions.assertEquals(testThrowEndAck.toString(), returnThrowEndAck.toString());;
    }

    @Test
    void incorrectTestProtocol() throws EncodingException {
        ThrowEndAckObject testThrowEndAck = new ThrowEndAckObject(testTimeID, testUnsuccess);
        BytePacket packet = testThrowEndAck.getBytePacket();
        packet.takeByte();

        Assertions.assertThrows(ParsePacketException.class, () -> {
            new ThrowEndAckObject(packet, testMacAddress);
        });
    }
}