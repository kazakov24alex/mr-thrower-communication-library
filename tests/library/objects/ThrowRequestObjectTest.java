package library.objects;

import library.BytePacket;
import library.ByteUtils;
import library.codes.PacketCodes;
import library.exceptions.BuildPacketException;
import library.exceptions.EncodingException;
import library.exceptions.ParsePacketException;
import library.wrappers.MD5;
import library.wrappers.MacAddress;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

class ThrowRequestObjectTest {


    private MacAddress testMacAddress;

    private int TEST_FILE_NUM = 3;
    private ThrowFileObject testThrowFile;

    private Date testTimeID;
    private String testFilename;
    private long testFileSize;
    private MD5 testMD5;


    @BeforeEach
    void setUp() throws EncodingException {
        byte[] macBytes = { 0, 1, 2, 3, 4, 5 };
        byte[] md5Bytes = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };

        testMacAddress = new MacAddress(macBytes);

        testTimeID = new Date();
        testFilename = "TEST_file_имя";
        testFileSize = 77777777;
        testMD5 = new MD5(md5Bytes);

        testThrowFile = new ThrowFileObject(testTimeID, testFilename, testFileSize, testMD5);
    }

    @AfterEach
    void tearDown() {
    }


    @Test
    void testPOJO() {
        ThrowRequestObject request = new ThrowRequestObject();
        for(int i = 0; i < TEST_FILE_NUM; i++) {
            request.addThrowFile(testThrowFile);
        }
        request.setAddress(testMacAddress);

        Assertions.assertEquals(TEST_FILE_NUM, request.getThrowFiles().size());
        Assertions.assertEquals(0, testMacAddress.compareTo(request.getAddress()));
        for(int i = 0; i < TEST_FILE_NUM; i++) {
            Assertions.assertEquals(testThrowFile.getTimeID(), request.getThrowFiles().get(i).getTimeID());
            Assertions.assertEquals(testThrowFile.getFilename(), request.getThrowFiles().get(i).getFilename());
            Assertions.assertEquals(testThrowFile.getFileSize(), request.getThrowFiles().get(i).getFileSize());
            Assertions.assertEquals(0, testThrowFile.getMD5().compareTo(request.getThrowFiles().get(i).getMD5()));
            Assertions.assertEquals(0, testThrowFile.getAddress().compareTo(request.getThrowFiles().get(i).getAddress()));
        }
    }


    @Test
    void tesToString() {
        ThrowRequestObject requestFirst = new ThrowRequestObject();
        for(int i = 0; i < TEST_FILE_NUM; i++) {
            requestFirst.addThrowFile(testThrowFile);
        }
        ThrowRequestObject requestSecond = new ThrowRequestObject();
        for(int i = 0; i < TEST_FILE_NUM; i++) {
            requestSecond.addThrowFile(testThrowFile);
        }

        Assertions.assertEquals(requestFirst.toString(), requestSecond.toString());

        ThrowRequestObject requestThird = new ThrowRequestObject();
        for(int i = 0; i < TEST_FILE_NUM; i++) {
            requestThird.addThrowFile(testThrowFile);
        }
        requestThird.setAddress(testMacAddress);
        ThrowRequestObject requestFourth = new ThrowRequestObject();
        for(int i = 0; i < TEST_FILE_NUM; i++) {
            requestFourth.addThrowFile(testThrowFile);
        }
        requestFourth.setAddress(testMacAddress);

        Assertions.assertEquals(requestThird.toString(), requestFourth.toString());
    }


    @Test
    void testThrowFile() throws BuildPacketException, ParsePacketException, EncodingException {
        ThrowRequestObject testRequest = new ThrowRequestObject();
        for(int i = 0; i < TEST_FILE_NUM; i++) {
            testRequest.addThrowFile(testThrowFile);
        }
        testRequest.setAddress(testMacAddress);

        BytePacket packet = testRequest.getBytePacket();
        ThrowRequestObject returnThrowRequest = new ThrowRequestObject(packet, testMacAddress);

        byte code = packet.getByte(0);
        short recordedLength = ByteUtils.toShort(packet.getBytes(1, 2));
        Assertions.assertEquals(PacketCodes.THROW_REQUEST, code);
        Assertions.assertEquals(packet.getData().length, recordedLength);

        Assertions.assertEquals(testRequest.getThrowFiles().size(), returnThrowRequest.getThrowFiles().size());
        Assertions.assertEquals(testRequest.toString(), returnThrowRequest.toString());

        for(int i = 0; i < testRequest.getThrowFiles().size(); i++) {
            Assertions.assertEquals(testRequest.getThrowFiles().get(i).getFilename(), returnThrowRequest.getThrowFiles().get(i).getFilename());
            Assertions.assertEquals(testRequest.getThrowFiles().get(i).getFileSize(), returnThrowRequest.getThrowFiles().get(i).getFileSize());
            Assertions.assertEquals(testRequest.getThrowFiles().get(i).getMD5().compareTo(returnThrowRequest.getThrowFiles().get(i).getMD5()),0);
            Assertions.assertEquals(testRequest.getThrowFiles().get(i).getTimeID(), returnThrowRequest.getThrowFiles().get(i).getTimeID());
            Assertions.assertEquals(testRequest.getThrowFiles().get(i).toString(), returnThrowRequest.getThrowFiles().get(i).toString());
        }

    }

    @Test
    void incorrectThrowFileParse() throws BuildPacketException, EncodingException {
        ThrowRequestObject testRequest = new ThrowRequestObject();
        for(int i = 0; i < TEST_FILE_NUM; i++) {
            testRequest.addThrowFile(testThrowFile);
        }

        BytePacket packet = testRequest.getBytePacket();
        packet.takeByte();

        Assertions.assertThrows(ParsePacketException.class, () -> {
            new ThrowRequestObject(packet, testMacAddress);
        });
    }


    @Test
    void incorrectEmptyThrowFileParse() throws BuildPacketException, EncodingException {
        ThrowRequestObject testRequest = new ThrowRequestObject();

        Assertions.assertThrows(BuildPacketException.class, () -> {
            BytePacket packet = testRequest.getBytePacket();
        });
    }


}