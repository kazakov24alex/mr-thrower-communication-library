package library.objects;

import library.BytePacket;
import library.ByteUtils;
import library.codes.PacketCodes;
import library.exceptions.BuildPacketException;
import library.exceptions.EncodingException;
import library.exceptions.ParsePacketException;
import library.wrappers.MacAddress;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class GoodbyeObjectTest {

    private MacAddress testMacAddress;


    @BeforeEach
    void setUp() throws EncodingException {
        byte[] macBytes = { 0, 1, 2, 3, 4, 5 };
        testMacAddress = new MacAddress(macBytes);

    }

    @AfterEach
    void tearDown() {
    }


    @Test
    void testPOJO() {
        GoodbyeObject goodbyeOne = new GoodbyeObject(testMacAddress);

        Assertions.assertEquals(0, goodbyeOne.getMac().compareTo(testMacAddress));
    }


    @Test
    void testToString() {
        GoodbyeObject goodbyeOne = new GoodbyeObject(testMacAddress);
        GoodbyeObject goodbyeTwo = new GoodbyeObject(testMacAddress);

        Assertions.assertEquals(goodbyeOne.toString(), goodbyeTwo.toString());
    }


    @Test
    void testProtocol() throws ParsePacketException, EncodingException {
        GoodbyeObject testGoodbye = new GoodbyeObject(testMacAddress);
        BytePacket packet = testGoodbye.getBytePacket();
        GoodbyeObject returnGoodbye = new GoodbyeObject(packet);

        byte code = packet.getByte(0);
        short recordedLength = ByteUtils.toShort(packet.getBytes(1, 2));
        Assertions.assertEquals(PacketCodes.GOODBYE, code);
        Assertions.assertEquals(packet.getData().length, recordedLength);

        Assertions.assertEquals(0, testGoodbye.getMac().compareTo(returnGoodbye.getMac()));
    }


    @Test
    void incorrectTestProtocol() throws BuildPacketException, EncodingException {
        GoodbyeObject testGreeting = new GoodbyeObject(testMacAddress);
        BytePacket packet = testGreeting.getBytePacket();
        packet.takeByte();

        Assertions.assertThrows(ParsePacketException.class, () -> {
            new GoodbyeObject(packet);
        });
    }

}