package library.wrappers;

import library.exceptions.EncodingException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import java.net.SocketException;
import java.net.UnknownHostException;

import static com.sun.org.apache.xalan.internal.lib.ExsltStrings.split;


class MacAddressTester {

    private byte[] TEST_MAC_BYTES = { 84, 39, 30, 118, 26, 95 };
    private Byte[] TEST_MAC_BYTES_OBJ = { 84, 39, 30, 118, 26, 95 };
    private String TEST_MAC_STRING = "54:27:1E:76:1A:5F";


    @BeforeEach
    void setUp() { }

    @AfterEach
    void tearDown() { }


//======================================================================================================================
// GETTERS
//======================================================================================================================

    @Test
    void testToString() throws EncodingException, SocketException, UnknownHostException {
        MacAddress macAddress = new MacAddress(TEST_MAC_BYTES_OBJ);

        String macString = macAddress.toString();
        Assertions.assertEquals(macString, TEST_MAC_STRING);
    }

    @Test
    void incorrectTestToString() {
        MacAddress macAddress = new MacAddress();

        Assertions.assertEquals(macAddress.toString(), null);
    }

    @Test
    void testGetBytes()  throws EncodingException {
        MacAddress macAddress = new MacAddress(TEST_MAC_BYTES);

        Assertions.assertEquals(TEST_MAC_BYTES.length, macAddress.getBytes().length);
        for(int i = 0; i < TEST_MAC_BYTES.length; i++) {
            Assertions.assertEquals(macAddress.getBytes()[i], TEST_MAC_BYTES[i]);
        }
    }


//======================================================================================================================
// SETTERS
//======================================================================================================================

    @Test
    void testSetMac() throws EncodingException {
        MacAddress macAddress = new MacAddress();
        macAddress.setMac(TEST_MAC_BYTES);

        Assertions.assertEquals(TEST_MAC_BYTES.length, macAddress.getBytes().length);
        for(int i = 0; i < TEST_MAC_BYTES.length; i++) {
            Assertions.assertEquals(macAddress.getBytes()[i], TEST_MAC_BYTES[i]);
        }
    }

    @Test
    void testSetMacString() throws EncodingException {
        MacAddress macAddress = new MacAddress(TEST_MAC_STRING);

        Assertions.assertEquals(TEST_MAC_STRING, macAddress.toString());
    }

    @Test
    void testSetMacBytesIncorrect() {
        Assertions.assertThrows(EncodingException.class, () -> {
            byte[] incorrectMac = { 84, 39, 30, 118, 26 };
            MacAddress macAddress = new MacAddress();
            macAddress.setMac(incorrectMac);
        });

        Assertions.assertThrows(EncodingException.class, () -> {
            byte[] incorrectMac = { 84, 39, 30, 118, 26, 95, 1 };
            MacAddress macAddress = new MacAddress();
            macAddress.setMac(incorrectMac);
        });
    }

    @Test
    void testSetMacStringIncorrect() throws EncodingException {
        Assertions.assertThrows(EncodingException.class, () -> {
            MacAddress macAddress = new MacAddress("5427:1E:76:1A:5F");
        });

        Assertions.assertThrows(EncodingException.class, () -> {
            MacAddress macAddress = new MacAddress("54:27:1E:76:1A:5G");
        });
    }


//======================================================================================================================
// COMPARABLE
//======================================================================================================================

    @Test
    void testCompareTo() throws EncodingException {
        byte[] mac1 = { 84, 39, 30, 118, 26, 0 };
        byte[] mac2 = { 84, 39, 30, 118, 26, 95 };
        byte[] mac3 = { 84, 39, 30, 118, 26, 127 };

        MacAddress macAddress1 = new MacAddress(mac1);
        MacAddress macAddress2 = new MacAddress(mac2);
        MacAddress macAddress2other = new MacAddress(mac2);
        MacAddress macAddress3 = new MacAddress(mac3);

        Assertions.assertEquals(macAddress2.compareTo(macAddress1), 1);
        Assertions.assertEquals(macAddress2.compareTo(macAddress3), -1);
        Assertions.assertEquals(macAddress2.compareTo(macAddress2other), 0);
    }


}

