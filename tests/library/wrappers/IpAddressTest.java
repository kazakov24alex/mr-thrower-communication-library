package library.wrappers;

import library.exceptions.EncodingException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;


class IpAddressTest  {

    private byte[] TEST_IP_BYTES = { -1, 0, 127 , -128 };
    private Byte[] TEST_IP_BYTES_OBJ = { -1, 0, 127 , -128 };
    private short[] TEST_IP_SHORTS = { 255, 0, 127, 128 };
    private String TEST_IP_STRING = "255.0.127.128";


    @BeforeEach
    void setUp() { }

    @AfterEach
    void tearDown() { }


//======================================================================================================================
// GETTERS
//======================================================================================================================

    @Test
    void testToString() throws EncodingException {
        IpAddress ipAddress = new IpAddress(TEST_IP_BYTES);

        String ipString = ipAddress.toString();
        Assertions.assertEquals(ipString, TEST_IP_STRING);
    }

    @Test
    void testGetBytes()  throws EncodingException {
        IpAddress ipAddress = new IpAddress(TEST_IP_BYTES_OBJ);

        Assertions.assertEquals(TEST_IP_BYTES.length, ipAddress.getBytes().length);
        for(int i = 0; i < TEST_IP_BYTES.length; i++) {
            Assertions.assertEquals(ipAddress.getBytes()[i], TEST_IP_BYTES[i]);
        }
    }

    @Test
    void getShorts() throws EncodingException {
        IpAddress ipAddress = new IpAddress(TEST_IP_SHORTS);

        Assertions.assertEquals(TEST_IP_SHORTS.length, ipAddress.getShorts().length);
        for(int i = 0; i < TEST_IP_SHORTS.length; i++) {
            Assertions.assertEquals(ipAddress.getShorts()[i], TEST_IP_SHORTS[i]);
        }
    }


//======================================================================================================================
// SETTERS
//======================================================================================================================

    @Test
    void testSetIpBytes() throws EncodingException {
        IpAddress ipAddress = new IpAddress();
        ipAddress.setIp(TEST_IP_BYTES);

        Assertions.assertEquals(TEST_IP_BYTES.length, ipAddress.getBytes().length);
        for(int i = 0; i < TEST_IP_BYTES.length; i++) {
            Assertions.assertEquals(ipAddress.getBytes()[i], TEST_IP_BYTES[i]);
        }
    }

    @Test
    void testSetIpBytesIncorrect() {
        Assertions.assertThrows(EncodingException.class, () -> {
            byte[] incorrectIp = { -1, 0, 127 };
            IpAddress ipAddress = new IpAddress();
            ipAddress.setIp(incorrectIp);
        });

        Assertions.assertThrows(EncodingException.class, () -> {
            byte[] incorrectIp = { -1, 0, 127, -128, 1 };
            IpAddress ipAddress = new IpAddress();
            ipAddress.setIp(incorrectIp);
        });
    }

    @Test
    void testSetIpShorts() throws EncodingException {
        IpAddress ipAddress = new IpAddress();
        ipAddress.setIp(TEST_IP_SHORTS);

        Assertions.assertEquals(TEST_IP_SHORTS.length, ipAddress.getShorts().length);
        for(int i = 0; i < TEST_IP_SHORTS.length; i++) {
            Assertions.assertEquals(ipAddress.getShorts()[i], TEST_IP_SHORTS[i]);
        }
    }

    @Test
    void testSetIpString() throws EncodingException {
        IpAddress ipAddress = new IpAddress(TEST_IP_STRING);

        Assertions.assertEquals(TEST_IP_STRING, ipAddress.toString());
    }

    @Test
    void testSetIpShortsIncorrect() {
        Assertions.assertThrows(EncodingException.class, () -> {
            short[] incorrectIp = { 255, 0, 127, 256 };
            IpAddress ipAddress = new IpAddress();
            ipAddress.setIp(incorrectIp);
        });

        Assertions.assertThrows(EncodingException.class, () -> {
            short[] incorrectIp = { 255, 0, 127, -1 };
            IpAddress ipAddress = new IpAddress();
            ipAddress.setIp(incorrectIp);
        });

        Assertions.assertThrows(EncodingException.class, () -> {
            short[] incorrectIp = { 255, 0, 127 };
            IpAddress ipAddress = new IpAddress();
            ipAddress.setIp(incorrectIp);
        });

        Assertions.assertThrows(EncodingException.class, () -> {
            short[] incorrectIp = { 255, 0, 127, 128, 0 };
            IpAddress ipAddress = new IpAddress();
            ipAddress.setIp(incorrectIp);
        });

        Assertions.assertThrows(EncodingException.class, () -> {
            String incorrectIp = "192.168.1";
            IpAddress ipAddress = new IpAddress(incorrectIp);
        });

        Assertions.assertThrows(EncodingException.class, () -> {
            String incorrectIp = "192.168.num.241";
            IpAddress ipAddress = new IpAddress(incorrectIp);
        });


    }


//======================================================================================================================
// COMPARABLE
//======================================================================================================================

    @Test
    void testCompareTo() throws EncodingException {
        short[] ip1 = { 255, 0, 127, 0 };
        short[] ip2 = { 255, 0, 127, 128 };
        short[] ip3 = { 255, 0, 127, 255 };

        IpAddress ipAddress1 = new IpAddress(ip1);
        IpAddress ipAddress2 = new IpAddress(ip2);
        IpAddress ipAddress2other = new IpAddress(ip2);
        IpAddress ipAddress3 = new IpAddress(ip3);

        Assertions.assertEquals(ipAddress2.compareTo(ipAddress1), 1);
        Assertions.assertEquals(ipAddress2.compareTo(ipAddress3), -1);
        Assertions.assertEquals(ipAddress2.compareTo(ipAddress2other), 0);
    }


}