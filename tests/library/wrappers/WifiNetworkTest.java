package library.wrappers;

import library.codes.*;
import library.exceptions.EncodingException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.SocketException;
import java.net.UnknownHostException;

import static org.junit.jupiter.api.Assertions.*;


class WifiNetworkTest {

    private String TEST_SSID = "ROSTELECOM_781B";
    private String TEST_NETWORK_TYPE = "Infrastructure";
    private String TEST_AUTHENTICATION_WPA = "WPA2-Personal";
    private String TEST_AUTHENTICATION_OPEN = "some authentication";
    private String TEST_ENCRYPTION = "CCMP";

    private WifiNetwork wifiNetworkWPA;
    private WifiNetwork wifiNetworkOpen;


    @BeforeEach
    void setUp() {
        wifiNetworkWPA = new WifiNetwork(
                TEST_SSID,
                TEST_NETWORK_TYPE,
                TEST_AUTHENTICATION_WPA,
                TEST_ENCRYPTION);

        wifiNetworkOpen = new WifiNetwork(
                TEST_SSID,
                TEST_NETWORK_TYPE,
                TEST_AUTHENTICATION_OPEN,
                TEST_ENCRYPTION);
    }

    @AfterEach
    void tearDown() { }



//======================================================================================================================
// GETTERS
//======================================================================================================================

    @Test
    void testToString() {
        String wifiNrtworkOriginalString = "\nSSID: " + TEST_SSID + "\n" +
                "\tNetwork type:\t" + TEST_NETWORK_TYPE + "\n" +
                "\tAuthentication:\t" + TEST_AUTHENTICATION_WPA + "\n" +
                "\tEncryption:\t\t" + TEST_ENCRYPTION;

        Assertions.assertEquals(wifiNetworkWPA.toString(), wifiNrtworkOriginalString);
    }

    @Test
    void testIsPasswordTrueProtected() {
        Assertions.assertEquals(wifiNetworkWPA.isPasswordProtected(), true);
    }

    @Test
    void testIsPasswordFalseProtected() {
        Assertions.assertEquals(wifiNetworkOpen.isPasswordProtected(), false);
    }

    @Test
    void testGetSsid() {
        Assertions.assertEquals(wifiNetworkWPA.getSsid(), TEST_SSID);
    }

    @Test
    void testGetNetworkType() {
        Assertions.assertEquals(wifiNetworkWPA.getNetworkType(), TEST_NETWORK_TYPE);
    }

    @Test
    void testGetAuthentication() {
        Assertions.assertEquals(wifiNetworkWPA.getAuthentication(), TEST_AUTHENTICATION_WPA);
    }

    @Test
    void testGetEncryption() {
        Assertions.assertEquals(wifiNetworkWPA.getEncryption(), TEST_ENCRYPTION);
    }


    @Test
    void codesTests() {
        new DirectionCodes();
        new OsCodes();
        new ThrowModeCodes();
        new ThrowStateCodes();
        new ThrowTypeCodes();
    }

}