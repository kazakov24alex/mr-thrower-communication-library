package library.wrappers;

import library.exceptions.EncodingException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

class MD5Test {

    private byte[] TEST_MD5_BYTES = { -128, -127, -126, -125, -124, -123, -2, -1, 0, 1, 2, 123, 124, 125, 126, 127 };
    private String TEST_MD5_STRING = "808182838485feff0001027b7c7d7e7f";


    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testToString() throws EncodingException, IOException, NoSuchAlgorithmException {
        MD5 md5 = new MD5(TEST_MD5_BYTES);

        Assertions.assertEquals(TEST_MD5_STRING, md5.toString());
    }

    @Test
    void testGetBytes() throws EncodingException {
        MD5 md5 = new MD5(TEST_MD5_BYTES);

        Assertions.assertEquals(TEST_MD5_BYTES.length, md5.getBytes().length);
        for(int i = 0; i < TEST_MD5_BYTES.length; i++) {
            Assertions.assertEquals(md5.getBytes()[i], TEST_MD5_BYTES[i]);
        }
    }

    @Test
    void testCompareTo() throws EncodingException {
        byte[] TEST_MD5_BYTES_LESS = { -128, -127, -126, -125, -124, -123, -2, -1, 0, 1, 2, 123, 124, 125, 125, 127 };
        byte[] TEST_MD5_BYTES_MORE = { -128, -127, -126, -125, -124, -123, -2, -1, 0, 1, 2, 123, 124, 125, 127, 127 };


        MD5 md5 = new MD5(TEST_MD5_BYTES);
        MD5 md5equal = new MD5(TEST_MD5_BYTES);
        MD5 md5less = new MD5(TEST_MD5_BYTES_LESS);
        MD5 md5more = new MD5(TEST_MD5_BYTES_MORE);


        Assertions.assertEquals(0, md5.compareTo(md5equal));
        Assertions.assertEquals(1, md5.compareTo(md5less));
        Assertions.assertEquals(-1, md5.compareTo(md5more));
    }

    @Test
    void incorrectSource() throws EncodingException {
        byte[] TEST_MD5_BYTES_TOO_MUCH = { -127, -126, -125, -124, -123, -2, -1, 0, 1, 2, 123, 124, 125, 126, 127, 1, 1, 2 };
        byte[] TEST_MD5_BYTES_TOO_FEW = { -127, -126, -125, -124, -123, -2, -1, 0, 1, 2, 123, 124, 125, 126, 127 };

        Assertions.assertThrows(EncodingException.class, () -> {
            new MD5(TEST_MD5_BYTES_TOO_MUCH);
        });

        Assertions.assertThrows(EncodingException.class, () -> {
            new MD5(TEST_MD5_BYTES_TOO_FEW);
        });
    }

    @Test
    void testInitFromString() throws EncodingException {
        MD5 md5 = new MD5(TEST_MD5_STRING);

        Assertions.assertEquals(TEST_MD5_STRING, md5.toString());
    }

}