package library;


import library.exceptions.EncodingException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class ByteUtilsTest {

    private byte[] TEST_BYTE_ARRAY = {0, -128, 127, 1, 90 };
    private Byte[] TEST_BYTE_OBJ_ARRAY = {0, -128, 127, 1, 90 };
    int value1int = 32767;


    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }


//======================================================================================================================
// TO BYTE-OBJECTS
//======================================================================================================================

    @Test
    void testBytesToObjects() {
        ByteUtils byteUtils = new ByteUtils();
        Byte[] newByteArray = byteUtils.bytesToObjects(TEST_BYTE_ARRAY);

        Assertions.assertEquals(newByteArray.length, TEST_BYTE_OBJ_ARRAY.length);
        for(int i = 0; i < TEST_BYTE_OBJ_ARRAY.length; i++) {
            Assertions.assertEquals(newByteArray[i], TEST_BYTE_OBJ_ARRAY[i]);
        }
    }

    @Test
    void testShortToBytes() {
        // 1st case: 32767
        short value1 = 32767;
        Byte[] bytes1 = ByteUtils.shortToBytes(value1);

        Byte[] shiftCheck1 = new Byte[] {
                (byte)(value1 >>> 8),
                (byte) value1 };

        Assertions.assertEquals(bytes1.length, 2);
        for(int i = 0; i < 2; i++) {
            Assertions.assertEquals(bytes1[i], shiftCheck1[i]);
        }

        // 2nd case: 0
        short value2 = 0;
        Byte[] bytes2 = ByteUtils.shortToBytes(value2);

        Byte[] shiftCheck2 = new Byte[] {
                (byte)(value2 >>> 8),
                (byte) value2 };

        Assertions.assertEquals(bytes2.length, 2);
        for(int i = 0; i < 2; i++) {
            Assertions.assertEquals(bytes2[i], shiftCheck2[i]);
        }

        // 3rd case: -32768
        short value3 = -32768;
        Byte[] bytes3 = ByteUtils.shortToBytes(value3);

        Byte[] shiftCheck3 = new Byte[] {
                (byte)(value3 >>> 8),
                (byte) value3 };

        Assertions.assertEquals(bytes3.length, 2);
        for(int i = 0; i < 2; i++) {
            Assertions.assertEquals(bytes3[i], shiftCheck3[i]);
        }
    }

    @Test
    void testIntToBytes() {
        // 1st case: 2147483647
        int value1 = 2147483647;
        Byte[] bytes1 = ByteUtils.intToBytes(value1);

        Byte[] shiftCheck1 = new Byte[] {
                (byte)(value1 >>> 24),
                (byte)(value1 >>> 16),
                (byte)(value1 >>> 8),
                (byte) value1};

        Assertions.assertEquals(bytes1.length, 4);
        for(int i = 0; i < 4; i++) {
            Assertions.assertEquals(bytes1[i], shiftCheck1[i]);
        }

        // 2nd case: 0
        int value2 = 0;
        Byte[] bytes2 = ByteUtils.intToBytes(value2);

        Byte[] shiftCheck2 = new Byte[] {
                (byte)(value2 >>> 24),
                (byte)(value2 >>> 16),
                (byte)(value2 >>> 8),
                (byte) value2};

        Assertions.assertEquals(bytes2.length, 4);
        for(int i = 0; i < 4; i++) {
            Assertions.assertEquals(bytes2[i], shiftCheck2[i]);
        }

        // 3rd case: -2147483648
        int value3 = -2147483648;
        Byte[] bytes3 = ByteUtils.intToBytes(value3);

        Byte[] shiftCheck3 = new Byte[] {
                (byte)(value3 >>> 24),
                (byte)(value3 >>> 16),
                (byte)(value3 >>> 8),
                (byte) value3};

        Assertions.assertEquals(bytes3.length, 4);
        for(int i = 0; i < 4; i++) {
            Assertions.assertEquals(bytes3[i], shiftCheck3[i]);
        }
    }

    @Test
    void testLongToBytes() {
        // 1st case: 2147483647
        long value1 = 2147483647;
        Byte[] bytes1 = ByteUtils.longToBytes(value1);

        Byte[] check1 = new Byte[] {
                (byte)(value1 >>> 56),
                (byte)(value1 >>> 48),
                (byte)(value1 >>> 40),
                (byte)(value1 >>> 32),
                (byte)(value1 >>> 24),
                (byte)(value1 >>> 16),
                (byte)(value1 >>> 8),
                (byte) value1};

        Assertions.assertEquals(bytes1.length, 8);
        for (int i = 0; i < 8; i++) {
            Assertions.assertEquals(bytes1[i], check1[i]);
        }

        // 2nd case: 0
        long value2 = 0;
        Byte[] bytes2 = ByteUtils.longToBytes(value2);

        Byte[] check2 = new Byte[] {
                (byte)(value2 >>> 56),
                (byte)(value2 >>> 48),
                (byte)(value2 >>> 40),
                (byte)(value2 >>> 32),
                (byte)(value2 >>> 24),
                (byte)(value2 >>> 16),
                (byte)(value2 >>> 8),
                (byte) value2};

        Assertions.assertEquals(bytes2.length, 8);
        for (int i = 0; i < 8; i++) {
            Assertions.assertEquals(bytes2[i], check2[i]);
        }

        // 1st case: 2147483647
        long value3 = -2147483648;
        Byte[] bytes3 = ByteUtils.longToBytes(value3);

        Byte[] check3 = new Byte[] {
                (byte)(value3 >>> 56),
                (byte)(value3 >>> 48),
                (byte)(value3 >>> 40),
                (byte)(value3 >>> 32),
                (byte)(value3 >>> 24),
                (byte)(value3 >>> 16),
                (byte)(value3 >>> 8),
                (byte) value3};

        Assertions.assertEquals(bytes3.length, 8);
        for (int i = 0; i < 8; i++) {
            Assertions.assertEquals(bytes3[i], check3[i]);
        }
    }

    @Test
    void testStringToBytes() throws EncodingException {
        String testString = "Тестовая ฿ string";

        Byte[] bytes = ByteUtils.stringToBytes(testString);
        String returnString = ByteUtils.toString(bytes);

        Assertions.assertEquals(returnString, testString);
    }


//======================================================================================================================
// FROM OBJECTS
//======================================================================================================================

    @Test
    void testToPrimitives() {
        byte[] newByteArray = ByteUtils.toPrimitives(TEST_BYTE_OBJ_ARRAY);

        Assertions.assertEquals(newByteArray.length, TEST_BYTE_ARRAY.length);
        for(int i = 0; i < TEST_BYTE_ARRAY.length; i++) {
            Assertions.assertEquals(newByteArray[i], TEST_BYTE_ARRAY[i]);
        }
    }

    @Test
    void testToShort() throws EncodingException {
        // 1st case: 32767
        short value1 = 32767;
        Byte[] bytes1 = ByteUtils.shortToBytes(value1);

        short returnValue1 = ByteUtils.toShort(bytes1);
        Assertions.assertEquals(returnValue1, value1);

        // 2nd case: 0
        short value2 = 0;
        Byte[] bytes2 = ByteUtils.shortToBytes(value2);

        short returnValue2 = ByteUtils.toShort(bytes2);
        Assertions.assertEquals(returnValue2, value2);

        // 3rd case: -32768
        short value3 = -32768;
        Byte[] bytes3 = ByteUtils.shortToBytes(value3);

        short returnValue3 = ByteUtils.toShort(bytes3);
        Assertions.assertEquals(returnValue3, value3);
    }

    @Test
    void testToInt() throws EncodingException {
        // 1st case: 2147483647
        int value1 = 2147483647;
        Byte[] bytes1 = ByteUtils.intToBytes(value1);

        int returnValue1 = ByteUtils.toInt(bytes1);
        Assertions.assertEquals(returnValue1, value1);

        // 2nd case: 0
        int value2 = 0;
        Byte[] bytes2 = ByteUtils.intToBytes(value2);

        int returnValue2 = ByteUtils.toInt(bytes2);
        Assertions.assertEquals(returnValue2, value2);

        // 3rd case: -2147483648
        int value3 = -2147483648;
        Byte[] bytes3 = ByteUtils.intToBytes(value3);

        int returnValue3 = ByteUtils.toInt(bytes3);
        Assertions.assertEquals(returnValue3, value3);
    }

    @Test
    void testToLong() throws EncodingException {
        // 1st case: 2147483647
        long value1 = 2147483647;
        Byte[] bytes1 = ByteUtils.longToBytes(value1);

        long returnValue1 = ByteUtils.toLong(bytes1);
        Assertions.assertEquals(returnValue1, value1);

        // 2nd case: 0
        long value2 = 0;
        Byte[] bytes2 = ByteUtils.longToBytes(value2);

        long returnValue2 = ByteUtils.toLong(bytes2);
        Assertions.assertEquals(returnValue2, value2);

        // 3rd case: -2147483648
        long value3 = -2147483648;
        Byte[] bytes3 = ByteUtils.longToBytes(value3);

        long returnValue3 = ByteUtils.toLong(bytes3);
        Assertions.assertEquals(returnValue3, value3);
    }

    @Test
    void testToString() throws EncodingException {
        String testString = "Тестовая ฿ string";

        Byte[] bytes = ByteUtils.stringToBytes(testString);
        String returnString = ByteUtils.toString(bytes);

        Assertions.assertEquals(returnString, testString);
    }


//======================================================================================================================
// FROM OBJECTS (INCORRECT TESTS)
//======================================================================================================================

    @Test
    void incorrectTestToShort()  {
        Byte[] bytes1 = { 1 };
        Assertions.assertThrows(EncodingException.class, () -> {
            ByteUtils.toShort(bytes1);
        });

        Byte[] bytes2 = { 1, 2, 3 };
        Assertions.assertThrows(EncodingException.class, () -> {
            ByteUtils.toShort(bytes2);
        });
    }

    @Test
    void incorrectTestToInt()  {
        Byte[] bytes1 = { 1, 2, 3 };
        Assertions.assertThrows(EncodingException.class, () -> {
            ByteUtils.toInt(bytes1);
        });

        Byte[] bytes2 = { 1, 2, 3, 4, 5 };
        Assertions.assertThrows(EncodingException.class, () -> {
            ByteUtils.toInt(bytes2);
        });
    }

    @Test
    void incorrectTestToLong()  {
        Byte[] bytes1 = { 1, 2, 3, 4, 5, 6, 7 };
        Assertions.assertThrows(EncodingException.class, () -> {
            ByteUtils.toLong(bytes1);
        });

        Byte[] bytes2 = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        Assertions.assertThrows(EncodingException.class, () -> {
            ByteUtils.toLong(bytes2);
        });
    }

    @Test
    void incorrectTestToString() {
        Assertions.assertThrows(EncodingException.class, () -> {
            String returnString = ByteUtils.toString(null);
        });
    }


//======================================================================================================================
// BYTE NUMBER GETTER
//======================================================================================================================

    @Test
    void testGetBytesNum() throws EncodingException {
        String str = "Доброе morning!";
        int strLen = 21;

        int returnLen = ByteUtils.getBytesNum(str);
        Assertions.assertEquals(returnLen, strLen);
    }


}