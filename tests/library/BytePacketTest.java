package library;

import library.codes.PacketCodes;
import library.exceptions.EncodingException;

import org.junit.jupiter.api.*;

class BytePacketTest {

    private byte TEST_CODE = 127;
    private byte[] TEST_DATA = {-34, 127, 0, -128, 3, 7, -99, 101 };
    private String TEST_STRING = "-34,127,0,-128,3,7,-99,101";


    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }


//======================================================================================================================
// GETTERS
//======================================================================================================================

    @Test
    void testGetData() {
        BytePacket packet = new BytePacket(TEST_DATA);
        byte[] returnBytes = packet.getData();

        Assertions.assertEquals(TEST_DATA.length, returnBytes.length);
        for(int i = 0; i < TEST_DATA.length; i++) {
            Assertions.assertEquals(TEST_DATA[i], returnBytes[i]);
        }
    }

    @Test
    void testSize() {
        BytePacket packet = new BytePacket(TEST_CODE);
        packet.append(TEST_DATA);

        Assertions.assertEquals(TEST_DATA.length + 1, packet.size());
    }

    @Test
    void testGetByte() throws EncodingException {
        byte[] lengthBytes = ByteUtils.toPrimitives(ByteUtils.shortToBytes((short) (1+2+TEST_DATA.length)));
        BytePacket packet = new BytePacket(TEST_CODE, lengthBytes, TEST_DATA);

        byte returnByte = packet.getByte(6);
        Assertions.assertEquals(TEST_DATA[6-3], returnByte);

        returnByte = packet.getByte(0);
        Assertions.assertEquals(TEST_CODE, returnByte);
    }

    @Test
    void testGetBytes() throws EncodingException {
        BytePacket packet = new BytePacket(TEST_DATA);
        Byte[] returnBytes = packet.getBytes(2, 3);

        Assertions.assertEquals(3, returnBytes.length);
        for(int i = 0; i < 3; i++) {
            Assertions.assertEquals((Byte) TEST_DATA[2+i], returnBytes[i]);
        }
    }

    @Test
    void testToString() {
        BytePacket packet = new BytePacket(TEST_DATA);
        String returnString = packet.toString();

        Assertions.assertEquals(TEST_STRING, returnString);
    }


//======================================================================================================================
// GETTERS (INCORRECT)
//======================================================================================================================

    @Test
    void incorrectTestGetByte() {
        BytePacket packet = new BytePacket(TEST_DATA);

        Assertions.assertThrows(EncodingException.class, () -> {
            packet.getByte(-1);
        });

        Assertions.assertThrows(EncodingException.class, () -> {
            packet.getByte(TEST_DATA.length);
        });
    }

    @Test
    void incorrectTestGetBytes() {
        BytePacket packet = new BytePacket(TEST_DATA);

        Assertions.assertThrows(EncodingException.class, () -> {
            packet.getBytes(0, TEST_DATA.length+1);
        });

        Assertions.assertThrows(EncodingException.class, () -> {
            packet.getBytes(TEST_DATA.length-1, 2);
        });
    }


//======================================================================================================================
// SET
//======================================================================================================================

    @Test
    void testSetBytes() {
        BytePacket packet = new BytePacket(TEST_CODE);
        packet.set(ByteUtils.bytesToObjects(TEST_DATA));

        byte[] returnBytes = packet.getData();
        Assertions.assertEquals(TEST_DATA.length, returnBytes.length);
        for(int i = 0; i < TEST_DATA.length; i++) {
            Assertions.assertEquals(TEST_DATA[i], returnBytes[i]);
        }
    }

    @Test
    void testSetPacket() {
        BytePacket sourcePacket = new BytePacket(TEST_DATA);
        BytePacket packet = new BytePacket(TEST_CODE);
        packet.set(sourcePacket);

        byte[] sourceBytes = sourcePacket.getData();
        byte[] returnBytes = packet.getData();
        Assertions.assertEquals(sourceBytes.length, returnBytes.length);
        for(int i = 0; i < sourceBytes.length; i++) {
            Assertions.assertEquals(sourceBytes[i], returnBytes[i]);
        }
    }

    @Test
    void testSetString() throws EncodingException {
        String testString = "TEST_STRING (СТРОКА)";
        BytePacket packet = new BytePacket(TEST_CODE);
        packet.set(testString);

        String returnString = ByteUtils.toString(ByteUtils.bytesToObjects(packet.getData()));
        Assertions.assertEquals(testString, returnString);
    }

    @Test
    void testStoreAndRestoreData() {
        BytePacket packet = new BytePacket(TEST_DATA);
        packet.storeData();
        Byte[] newBytes = { 1, 2, 3, 4, 5 };
        packet.set(newBytes);
        packet.restoreData();

        byte[] returnBytes = packet.getData();
        Assertions.assertEquals(TEST_DATA.length, returnBytes.length);
        for(int i = 0; i < TEST_DATA.length; i++) {
            Assertions.assertEquals(TEST_DATA[i], returnBytes[i]);
        }
    }


//======================================================================================================================
// APPEND
//======================================================================================================================

    @Test
    void testAppendbytes() throws EncodingException {
        byte[] appendingBytes = { 0, 127, -128, 1 };
        BytePacket packet = new BytePacket(TEST_DATA);
        packet.append(appendingBytes);

        Byte[] returnBytes = packet.getBytes(TEST_DATA.length, appendingBytes.length);
        Assertions.assertEquals(returnBytes.length, appendingBytes.length);
        for(int i = 0; i < returnBytes.length; i++) {
            Assertions.assertEquals((byte) returnBytes[i], appendingBytes[i]);
        }
    }

    @Test
    void testAppendBytes() throws EncodingException {
        Byte[] appendingBytes = { 0, 127, -128, 1 };
        BytePacket packet = new BytePacket(TEST_DATA);
        packet.append(appendingBytes);

        Byte[] returnBytes = packet.getBytes(TEST_DATA.length, appendingBytes.length);
        Assertions.assertEquals(returnBytes.length, appendingBytes.length);
        for(int i = 0; i < returnBytes.length; i++) {
            Assertions.assertEquals(returnBytes[i], appendingBytes[i]);
        }
    }

    @Test
    void testAppendPacket() throws EncodingException {
        BytePacket packet = new BytePacket(TEST_DATA);
        byte[] newBytes = { 1, 2, 3, 4, 5 };
        BytePacket newPacket = new BytePacket(newBytes);
        packet.append(newPacket);

        Byte[] returnBytes = packet.getBytes(TEST_DATA.length, newBytes.length);
        Assertions.assertEquals(returnBytes.length, newBytes.length);
        for(int i = 0; i < returnBytes.length; i++) {
            Assertions.assertEquals((byte) returnBytes[i], newBytes[i]);
        }
    }

    @Test
    void testAppendByte() throws EncodingException {
        Byte appendingByte = -128;
        BytePacket packet = new BytePacket(TEST_DATA);
        packet.appendByte(appendingByte);

        Byte returnByte = packet.getByte(TEST_DATA.length);
        Assertions.assertEquals(appendingByte, returnByte);
    }

    @Test
    void testAppendShort() throws EncodingException {
        BytePacket packet = new BytePacket(TEST_DATA);
        short testShort = 2018;
        packet.appendShort(testShort);

        Byte[] byteShort = packet.getBytes(TEST_DATA.length, 2);
        short returnShort = ByteUtils.toShort(byteShort);

        Assertions.assertEquals(testShort, returnShort);
    }

    @Test
    void testAppendInt() throws EncodingException {
        BytePacket packet = new BytePacket(TEST_DATA);
        int testInt = 2018;
        packet.appendInt(testInt);

        Byte[] byteInt = packet.getBytes(TEST_DATA.length, 4);
        int returnInt = ByteUtils.toInt(byteInt);

        Assertions.assertEquals(testInt, returnInt);
    }

    @Test
    void testAppendLong() throws EncodingException {
        BytePacket packet = new BytePacket(TEST_DATA);
        long testLong = 2000018;
        packet.appendLong(testLong);

        Byte[] byteLong = packet.getBytes(TEST_DATA.length, 8);
        long returnLong = ByteUtils.toLong(byteLong);

        Assertions.assertEquals(testLong, returnLong);
    }

    @Test
    void testAppendString() throws EncodingException {
        BytePacket packet = new BytePacket(TEST_DATA);
        String testString = "Test STRING (Строка)";
        packet.appendString(testString);

        Byte[] byteString = packet.getBytes(TEST_DATA.length, ByteUtils.getBytesNum(testString));
        String returnString = ByteUtils.toString(byteString);

        Assertions.assertEquals(testString, returnString);
    }


//======================================================================================================================
// TAKE
//======================================================================================================================

    @Test
    void testTakeByte() throws EncodingException {
        PacketCodes packetCodes = new PacketCodes();
        BytePacket packet = new BytePacket(packetCodes.GREETING);
        Byte testByte = 127;
        packet.appendByte(testByte);
        packet.append(TEST_DATA);

        Assertions.assertEquals(TEST_DATA.length + 1 + 1, packet.size());
        packet.takeByte();
        Byte returnByte = packet.takeByte();
        Assertions.assertEquals(testByte, returnByte);
        Assertions.assertEquals(TEST_DATA.length, packet.size());
    }

    @Test
    void testTakeShort() throws EncodingException {
        BytePacket packet = new BytePacket(TEST_CODE);
        short testShort = 2018;
        packet.appendShort(testShort);
        packet.append(TEST_DATA);

        Assertions.assertEquals(TEST_DATA.length + 1 + 2, packet.size());
        packet.takeByte();
        short returnShort = packet.takeShort();
        Assertions.assertEquals(testShort, returnShort);
        Assertions.assertEquals(TEST_DATA.length, packet.size());
    }

    @Test
    void testTakeInt() throws EncodingException {
        BytePacket packet = new BytePacket(TEST_CODE);
        int testInt = 2018;
        packet.appendInt(testInt);
        packet.append(TEST_DATA);

        Assertions.assertEquals(TEST_DATA.length + 1 + 4, packet.size());
        packet.takeByte();
        int returnInt = packet.takeInt();
        Assertions.assertEquals(testInt, returnInt);
        Assertions.assertEquals(TEST_DATA.length, packet.size());
    }

    @Test
    void testTakeLong() throws EncodingException {
        BytePacket packet = new BytePacket(TEST_CODE);
        long testLong = 2000018;
        packet.appendLong(testLong);
        packet.append(TEST_DATA);

        Assertions.assertEquals(TEST_DATA.length + 1 + 8, packet.size());
        packet.takeByte();
        long returnLong = packet.takeLong();
        Assertions.assertEquals(testLong, returnLong);
        Assertions.assertEquals(TEST_DATA.length, packet.size());
    }

    @Test
    void testTakeString() throws EncodingException {
        BytePacket packet = new BytePacket(TEST_CODE);
        String testString = "Test String (Строка)!";
        packet.appendString(testString);
        packet.append(TEST_DATA);

        Assertions.assertEquals(TEST_DATA.length + 1 + ByteUtils.getBytesNum(testString), packet.size());
        packet.takeByte();
        String returnString = packet.takeString(ByteUtils.getBytesNum(testString));
        Assertions.assertEquals(testString, returnString);
        Assertions.assertEquals(TEST_DATA.length, packet.size());
    }

    @Test
    void testTakeBytes() throws EncodingException {
        BytePacket packet = new BytePacket(TEST_CODE);
        Byte[] testBytes = { 1, 2, 3, 4, 5, 6, 7 };
        packet.append(testBytes);
        packet.append(TEST_DATA);

        Assertions.assertEquals(TEST_DATA.length + 1 + testBytes.length, packet.size());
        packet.takeByte();
        Byte[] returnBytes = packet.takeBytes(testBytes.length);
        Assertions.assertEquals(testBytes.length, returnBytes.length);
        for(int i = 0; i < testBytes.length; i++) {
            Assertions.assertEquals(testBytes[i], returnBytes[i]);
        }
    }


//======================================================================================================================
// TAKE (INCORRECT)
//======================================================================================================================

    @Test
    void incorrectTestTakeByte() throws EncodingException {
        BytePacket packet = new BytePacket(TEST_CODE);
        packet.takeByte();

        Assertions.assertThrows(EncodingException.class, () -> {
            packet.takeByte();
        });
    }

    @Test
    void incorrectTestTakeShort() throws EncodingException {
        BytePacket packet = new BytePacket(TEST_CODE);

        Assertions.assertThrows(EncodingException.class, () -> {
            packet.takeShort();
        });
    }

    @Test
    void incorrectTestTakeInt() throws EncodingException {
        BytePacket packet = new BytePacket(TEST_CODE);
        packet.appendShort((short) 2);

        Assertions.assertThrows(EncodingException.class, () -> {
            packet.takeInt();
        });
    }

    @Test
    void incorrectTestTakeLong() throws EncodingException {
        BytePacket packet = new BytePacket(TEST_CODE);
        packet.appendShort((short) 2);
        packet.appendInt(4);

        Assertions.assertThrows(EncodingException.class, () -> {
            packet.takeLong();
        });
    }

    @Test
    void incorrectTestTakeString() throws EncodingException {
        BytePacket packet = new BytePacket(TEST_CODE);
        packet.takeByte();
        String testString = "Test String (Строка)";
        packet.appendString(testString);

        Assertions.assertThrows(EncodingException.class, () -> {
            packet.takeString(ByteUtils.getBytesNum(testString) + 1);
        });
    }

    @Test
    void incorrectTestTakeBytes() throws EncodingException {
        BytePacket packet = new BytePacket(TEST_CODE);
        packet.takeByte();
        byte[] testBytes = { 1, 2, 3, 4, 5 };
        packet.append(testBytes);

        Assertions.assertThrows(EncodingException.class, () -> {
            packet.takeBytes(testBytes.length + 1);
        });
    }


}
